#!/usr/bin/env python3
def get_requirements():
    print("\nProgram Requirements:")
    print("1. Calculate home interior paint cost (w/o primer)")
    print("2. Must use float data types.")
    print("3. Must use SQFT_PER_GALLON constant (350)")
    print("4. Must use iteration structure (aka 'Loop')")
    print("5. Format, right-aligned numbers, and round to two decimal places.")
    print("5. Create at least five functions that are caled by the program")
    print("\ta. main(): calls at least two other functions.\n\tb. get_requirements(): displays the program requirements.\n\tc. estimate_painting_cost(): calculaties interior home painting, and calls print functions.\n\td. print_painitng_estimate(): displays painting cost\n\te. prinrt_painting_percentage(): displays painting cost percentages.")

def estimate_painting_cost():
    total_sqft = 0
    price_p_gall = 0
    hrs_gall_p_sqft = 0

    SQFT_PER_GALLON = 350
    lck = False #Keeps the input loop in cehck
    lck_overall = False #Keeps the overall loop in check

    #Collects the input
    while lck_overall == False:
        print("\nInput: ")
        lck = False
        while lck == False:
            try:
                total_sqft = float(input("Enter Enter total interior sq ft: "))
                lck = True
            except ValueError:
                lck = False
                print("You have entered an inccorect value. Please enter a dcimal number")
    
        lck = False
        while lck == False:
            try:
                price_p_gall = float(input("Enter price per gallon paint: "))
                lck = True
            except ValueError:
                lck = False
                print("You have entered an inccorect value. Please enter a dcimal number")
    
        lck = False
        while lck == False:
            try:
                hrs_gall_p_sqft = float(input("Enter hourly painting rate per sq ft: "))
                lck = True
            except ValueError:
                lck = False
                print("You have entered an inccorect value. Please enter a dcimal number")
    

        #Calculates Info
        gallon = (total_sqft / SQFT_PER_GALLON) #Finds gallons
        total_paint = (gallon * price_p_gall) #Paint price per gallon

        #Printing out the results
        print_painting_estimate(total_sqft, SQFT_PER_GALLON, gallon, price_p_gall, hrs_gall_p_sqft)

        lck = False
        while lck == False:
            repeat = input("\nEstimate another paint job? (y/n): ")
            repeat = repeat.lower()
            if repeat == "y":
                lck = True
            elif repeat == "n":
                print("Thank you for using our Painting Estimator!\nPlease see our web site: http://www.mysite.com")
                lck = True
                lck_overall = True
            else:
                print("You have inputed an invalid character")
                lck = False

def print_painting_estimate(sqft, sqft_gallon, gallon, price, hrs):
    print("\nOutput:")
    print ("{0:20} {1:20}".format("Item:" , "Ammount"))
    print ("{0:20} {1:>7.2f}".format("Total Sq Ft:" , sqft))
    print ("{0:20} {1:>7.2f}".format("Sq Ft per Gallon:" , sqft_gallon))
    print ("{0:20} {1:>7.2f}".format("Number of Gallons:" , gallon))
    print ("{0:20} {1:>7.2f}".format("Paint per Gallon:" , price))
    print ("{0:20} {1:>7.2f}".format("Labor per Sq Ft:" , hrs))

    print_painting_percentage(sqft, gallon, price, hrs)

def print_painting_percentage(sqft, gallon, price, hrs):
    
    paint = gallon * price
    labor = sqft * hrs
    total = paint + labor

    paint_per = (paint / total)
    labor_per = (labor / total)
    total_per = (total / total)

    print("\n")
    print("{0:<10} {1:>10} {2:>10}".format("Cost" , " Amount" , " Percentage"))
    print("{0:<10} ${1:>10,.2f} {2:>10.2%}".format("Paint:" , paint , paint_per))
    print("{0:<10} ${1:>10,.2f} {2:>10.2%}".format("Labor:" , labor , labor_per))
    print("{0:<10} ${1:>10,.2f} {2:>10.2%}".format("Total:" , total , total_per))