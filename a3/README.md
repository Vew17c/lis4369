> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 Extensible Enterprise Solutions

## Vincent Williams

### Assignment 3 Requirements:

1. Use Selection Selectors
2. Create a paint estimator calculator in python
3. Have said calculator run sucessfully on both Python and VS Code


README.md file should include the following item(s):    
*Screenshot of paint estimator calculator with overtime pay calculated*
*Screenshot of paint estimator calculator without overtime pay calculated*

### Assignment Screenshots:

*Screenshot of calculator running in VS Code*:         
![Cal Screenshot IDLE](../img/a3_IDLE.png)

*Screenshot of calculator running in Python IDLE*:        
![Cal Screenshot VS Code](../img/a3_vs_code.png)  

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Vew17c/lis4368/src/master/bitbucketstationlocations.html)