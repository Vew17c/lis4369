> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 Extensible Enterprise Solutions

## Vincent Williams

### Assignment 5 Requirements:

1. Code and Run Demo in R
2. Backward engineer the screenshot
3. Trim the data pulled from XOM
4. Present the data in a readable fashion using multiple of graphs
5. Have the program run in RStudios


README.md file should include the following item(s):    
*Screenshot of program running on RStudio and Graphs*
*Screenshot of R Tutorial Demo*

### Assignment Screenshots:

*Screenshot of R tutorial*:         
![Cal Screenshot IDLE](../img/a5_R_demo.png)  

*Screenshot of the RStudio console running A5*:        
![Cal Screenshot VS Code](../img/a5_console.png)



*Screenshot of a Titanic Histogram*:         
![JDK Installation Screenshot](../img/a5_gra_1.png) 

*Screenshot of Boxplot between Suvivors and Age*:
![Android Studio Installation Screenshot](../img/a5_gra_2.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Vew17c/lis4368/src/master/bitbucketstationlocations.html)