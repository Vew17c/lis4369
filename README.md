> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 Extensible Enterprise Solutions

## Vincent Williams

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Python
    - Install R
    - Install R Studio Desktop
    - Install Visual Studio Code (or an alternative text editior)
    - Create a tip calculator in python
    - Have the calculator run on both the Pyhton IDLE and Visual studio code
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create functions
    - Creating a function and main file
    - Improting functions into main
    - Demosntrating the use of hierarcy and namespace
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Use selector statements
    - Creating a function and main file
    - Improting functions into main
    - Used a look up a table
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create 3 functions for data analysis 2
    - Install packages using pip
    - Trim the data pulled from XOM
    - Present the data in a readable fashion for the user
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Backward-engineer the program
    - Learn and understand how R works
    - Create boxplots and Histograms
    - Learn about the functions used in R
    - Trim the data pulled from XOM
6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Using DateTime
    - Install packages using pip
    - Trim the data pulled from XOM
    - Present the data in a readable fashion for the user
7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Backward-engineer the requirements
    - Demonstrate an understanding understand how R works
    - Create plots and qplots
    - Trim the data pulled from a CVS.


