#!/usr/bin/env python3
def get_requirements():
    print("\nProgram Requirements:")
    print("1. Run demo.py")
    print("2. Errors = Missing installations")
    print("3. Test Python Package Installer: pip freeze")
    print("4. Research how to do the following installations:")
    print("\ta. pandas (only if missing)\n\tb. pandas-datareader (only if missing)\n\tc. matplotlib (only if missing)")
    print("5. Create at least three functions that are caled by the program")
    print("\ta. main(): calls at least two other functions.\n\tb. get_requirements(): displays the program requirements.\n\tc. data_analysis_1(): display the following data.")

def data_analysis_1():
    import pandas as pd
    import datetime
    import pandas_datareader as pdr 
    import matplotlib.pyplot as plt 
    from matplotlib import style 

    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime(2018, 10, 15)

    df = pdr.DataReader("XOM" , "yahoo", start, end)

    last_p = len(df)

    print("\nPrint number of records: ")
    print(last_p)

    print("\nPrint columns")
    print(df.columns) #Why is it important to run this?

    print("\nPrint data frame: ")
    print(df[0:60]) #Print only 60

    print("\nPrint first five lines: ")
    print(df[0:5]) 

    print("\nPrint last five lines: ")
    print(df[(last_p - 5):last_p])

    print("\nPrint first 2 lines: ")
    print(df[0:2]) 

    print("\nPrint last 2 lines: ")
    print(df[(last_p - 2):last_p])


    #Research what these styles do
    # style.use('fivethirtyeight)
    # and compare it to
    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()