> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 Extensible Enterprise Solutions

## Vincent Williams

### Project 1 Requirements:

1. Code and Run Demo.py
2. Backward engineer the screenshot
3. Trim the data pulled from XOM
4. Present the data in a readable fashion using DataTime


README.md file should include the following item(s):    
*Screenshot of data_analysis working in both VS Code and Python IDLE*

### Assignment Screenshots:

*Screenshot of data_analysis VS Code*:         
![Cal Screenshot IDLE](../img/p1_ana1.png)  

*Screenshot of data_analysis in Python IDLE*:        
![Cal Screenshot VS Code](../img/p1_ana2.png)

*Screenshot of data_analysis graph*:        
![Cal Screenshot VS Code](../img/p1_graph.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Vew17c/lis4368/src/master/bitbucketstationlocations.html)