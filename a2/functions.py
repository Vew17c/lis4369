#!/usr/bin/env python3
def get_requirements():
    print("\nProgram Requirements:")
    print("1. Must use float data type for user input.")
    print("2. Overtime rate: 1.5 times hourly rate (hours over40).")
    print("3. Holiday rate: 2.0 times hourly rate(all holiday hours).")
    print("4. Must format currency with dollar sign, and round to two decimal places.")
    print("5. Create at least three functions that are caled by the program")
    print("\ta. main(): calls at least two other functions.\n\tb. get_requirements(): displays the program requirements.\n\tc. calulate_payroll(): calulates an idnividual one-week paycheeck.\n")

def calc_payroll():
    hours = 0
    holiday_hours = 0
    overtime = 0
    rate = 0
    total = 0

    MAX_HOURS = 40 #Constant for Max hours a person could work.
    OVERTIME_RATE = 1.5 #The adjusted overtime rate
    HOLIDAY_RATE = 2 #The adjusted holiday rate

    lck = False #Keeps the loop in cehck

    #Collects the input
    print("Input: ")

    while lck == False:
        try:
            hours = float(input("Enter hours worked: "))
            lck = True
        except ValueError:
            lck = False
            print("You have entered an inccorect value. Please enter a dcimal number")
    
    lck = False
    while lck == False:
        try:
            holiday_hours = float(input("Enter holiday hours: "))
            lck = True
        except ValueError:
            lck = False
            print("You have entered an inccorect value. Please enter a dcimal number")
    
    lck = False
    while lck == False:
        try:
            rate = float(input("Enter hourly pay rate: "))
            lck = True
        except ValueError:
            lck = False
            print("You have entered an inccorect value. Please enter a dcimal number")
    

    #Calculates Overtime
    overtime = hours - MAX_HOURS

    if overtime < 0:
        overtime = 0

    #Calculates hours
    hours = hours - overtime

    #Total pay according to the hours
    hours = hours * rate

    #Calculates overtime
    overtime = overtime * (rate * OVERTIME_RATE)

    #Calculating Holiday Hours
    holiday_hours = holiday_hours * (rate * HOLIDAY_RATE)

    #Finds the total
    total = hours + overtime + holiday_hours

    #Printing out the results
    print_pay(hours, holiday_hours, overtime, total)

def print_pay(hours, holoday_hours, overtime, pay):
    print("\nOutput:")
    print ("Base:     " + '${:,.2f}'.format(round(hours)))
    print ("Overtime: " + '${:,.2f}'.format(round(overtime)))
    print ("Holiday:  " + '${:,.2f}'.format(round(holoday_hours)))
    print ("Gross:    " + '${:,.2f}'.format(round(pay)))
