> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 Extensible Enterprise Solutions

## Vincent Williams

### Assignment 2 Requirements:

1. Backward-engineer (using Python) the following screenshots: 
2. The program should be organized with two modules (See Ch. 4):
    a. functions.py module contains the following functions: get_requirements(), calculate_payroll(), print_pay 
    b. main.py module imports the functions.py module, and calls the functions.  
 



README.md file should include the following items:    
*Screenshot of payroll calculator with overtime pay calculated*
*Screenshot of payroll calculator without overtime pay calculated*

### Assignment Screenshots:

*Screenshot of calculator calculating no overtime pay (In V.Studio Code*:
![Cal Screenshot w/o overtime pay](../img/a2_normal.png)

*Screenshot of calculator calulating overtime pay (In Python IDLE)*:
![Cal Screenshot w overtime pay](../img/a2_overtime.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Vew17c/lis4368/src/master/bitbucketstationlocations.html)