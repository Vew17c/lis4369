#!/usr/bin/env python3
import functions as f

def test(x = 5 , y = 2):
    return x+y

def main():
    f.get_requirements()
    f.calc_payroll()

if __name__ == "__main__":
    main()