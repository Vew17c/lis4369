> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 Extensible Enterprise Solutions

## Vincent Williams

### Assignment 1 Requirements:

1. Distributed Control Setup 
2. Development Instillations
3. Create a tip calculator in python
4. Have said calculator run sucessfully on both Python and VS Code



| README.md file should include the following items:    | Git commands w/short descriptions:
|:--                                                    |:--
| * Screenshot of Calcualtor running in Python IDLE     | 1. git init: Create an empty Git repository or reinitialize an existing one.
| * Screenshot of Calcualtor running in VS Code         | 2. git status: Show the working tree status.
| * Git Commands                                        | 3. git add: Add file contents to the index.
|                                                       | 4. git commit: Saves the changes to the repository.
|                                                       | 5. git push: Updates remote refs along with associated objects.
|                                                       | 6. git pull: Fetch from / integrate with another repository / a local branch.
|                                                       | 7. git config: Configures the author's name and email to your commits.

### Assignment Screenshots:

*Screenshot of calculator running in Python IDLE*:
![Cal Screenshot IDLE](../img/a1_python_shell.png)

*Screenshot of calculator running in Python IDLE*:
![Cal Screenshot VS Code](../img/a1_visual_shell.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Vew17c/lis4368/src/master/bitbucketstationlocations.html)