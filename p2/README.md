> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 Extensible Enterprise Solutions

## Vincent Williams

### Project 2 Requirements:

1. Code and Run program in R
2. Backward engineer the screenshot
3. Trim the data pulled from URL
4. Present the data in a readable fashion in the console, using various methods
5. Create 2 graphs
6. Have the program run in RStudios

README.md file should include the following item(s):    
*Screenshot of program running on RStudio and Graphs*
*Screenshot of R Tutorial Demo*

### Assignment Screenshots:

*Screenshot of the RStudio console running P2*:  
     
![RStudio Console](../img/p2_console.png)

*Screenshot of a Car Plot*:         

![Plot graph](../img/p2_gra_2.png) 

*Screenshot of Car QPlot*:

![QPlot graph](../img/p2_gra_1.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Vew17c/lis4368/src/master/bitbucketstationlocations.html)