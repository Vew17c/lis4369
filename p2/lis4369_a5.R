
a <- 9
a #Prints value of a

a + 5 #Prints a + 5

b <- sqrt(a)
b

c <- c(1,2,5,3,6,-2,4)
print(c)

typeof(c) #Prints datatype

is.list(c)
is.vector(c)

d <- c("One", "two", "three")
d

typeof(d)

e <- c(TRUE, TRUE, TRUE, FALSE, TRUE, FALSE) #Logical vector
e

typeof(e)

d[1]

my_str <- "Hello World!"
my_str

typeof(my_str)

sqrt(a)

sqrt(c)

a^2

c^2

min(c)

max(c)

mean(c)

sum(c)

url = "https://raw.github.com/vincentarelbundock/Rdatasets/master/csv/Stat2Data/Titanic.csv"
titanic <- read.csv(file=url, head=TRUE, sep=",")

titanic

summary(titanic) #Summary(): generic function used to produce results summaries

dir()

getwd()

names(titanic)

titanic$Name

titanic$Age

attributes(titanic)

ls()

mean(titanic$Age)

mean(titanic$Age, na.rm=TRUE)
median(titanic$Age, na.rm=TRUE)
quantile(titanic$Age, na.rm=TRUE)
min(titanic$Age, na.rm=TRUE)
max(titanic$Age, na.rm=TRUE)
var(titanic$Age, na.rm=TRUE)
sd(titanic$Age, na.rm=TRUE)

summary(titanic$Age, na.rm=TRUE)

titanic[!complete.cases(titanic),]

titanic_no_missing_data<- na.omit(titanic)
titanic_no_missing_data

help(stripchart)
stripchart(titanic_no_missing_data$Age)

hist(titanic_no_missing_data$Age, man="Distribution of Titanic Passanger Ages", xlab="Ages", col=c('green4'))

boxplot(titanic_no_missing_data$Age)

boxplot(titanic_no_missing_data, man="Distribution of Titanic Passanger Ages", xlab="Ages", horizontal=TRUE, col=c('lightsalmon2'))

plot(titanic_no_missing_data$Age, titanic_no_missing_data$Survived, man="Relationship Between Ages and Survival", xlab="Ages", ylab="Survived", col=c('hotpink4'))