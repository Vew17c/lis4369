<?php
//Note: JavaDoc-like comment syntax
//https://www.oracle.com/technetwork/java/javase/documentation/index-137868.html

/**
* Function Utility Library
*
* Package contains functions used for manipulating data.
*
* @package global
* @subpackage none: included only for example
*/

//used to display errors
include_once("error_display.php");

// ##### BEGIN getResultSet #####

/**
* Get result sets
*
* @author Mark Jowett <mjowett@fsu.edu>
	* @param string $query
	* @return string
	*/
	function getResultSet($query)
	{
  //make $db available inside function
  global $db;

	//Note: (Bootstrap) responsive DataTables automatically sort by first column in displayed table)

  try 
  {
	//with large query result sets, loop through fetch() rather than fetchAll():
	//fetchAll() returns array of arrays
	//http://php.net/manual/en/pdostatement.bindcolumn.php
	
	//because no user entered data, no need to bind values
	$statement = $db->prepare($query);
	$statement->execute();
	$statement->setFetchMode(PDO::FETCH_ASSOC);
	$result = $statement->fetchAll();
	$statement->closeCursor();
	return $result;
  } 

  catch (PDOException $e) 
  {
  $error = $e->getMessage();
  display_db_error($error);
  }
	}
	// ##### END getResultSet #####

	// ##### BEGIN addCoursePref #####

	/**
	* Add course preference
	*
	* @author Mark Jowett <mjowett@fsu.edu>
	* @param int $ins_id_v
	* @param int $crs_id_v
	* @param string $crp_pref_v
	* @param string $crp_delivery_v
	* @param string $crp_notes_v
	*/

	//Note: does *not* require PK, as it is auto incremented!
function addCoursePref
(
	$ins_id_v,
	$crs_id_v,
	$crp_pref_v,
	$crp_delivery_v,
	$crp_notes_v
)
{
	global $db;
	$query =
		"INSERT INTO course_pref
				 (ins_id, crs_id, crp_pref, crp_delivery, crp_notes)
				 VALUES
				 ( :ins_id_p, :crs_id_p, :crp_pref_p, :crp_delivery_p, :crp_notes_p)";
	try
	{
		$statement = $db->prepare($query);
		$statement->bindParam(':ins_id_p',$ins_id_v);
		$statement->bindParam(':crs_id_p',$crs_id_v);
		$statement->bindParam(':crp_pref_p',$crp_pref_v);
		$statement->bindParam(':crp_pref_p',$crp_pref_v);
		$statement->bindParam(':crp_pref_p',$crp_pref_v);
		$statement->bindParam(':crp_pref_p',$crp_pref_v);
		$statement->bindParam(':crp_pref_p',$crp_pref_v);
		$statement->bindParam(':crp_pref_p',$crp_pref_v);
		$statement->bindParam(':crp_pref_p',$crp_pref_v);
		$statement->bindParam(':crp_pref_p',$crp_pref_v);
		$statement->bindParam(':crp_delivery_p',$crp_delivery_v);
		$statement->bindParam(':crp_notes_p',$crp_notes_v);
		$statement->execute();

	/*
	//Use these print statements to test variable contents--that is, is the correct data being processed from the user input page?
	echo $ins_id_v, ' ', $crs_id_v, ' ',$crp_pref_v, ' ',$crp_delivery_v , ' ',$crp_notes_v, PHP_EOL;
	echo $query;
	*/
	
		$statement->closeCursor();
		
		//$last_auto_increment_id = $db->lastInsertId();
	}
	
	catch(PDOEXception$e)
	{
		$error = $e->getMessage();
		display_db_error($error);
		
	}
	//print_r($db->errorInfo());
	
	//include('index.php'); //forwarding is faster, one trip to server
	header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)				
}
// ##### END addCoursePref #####
?>
