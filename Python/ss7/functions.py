 #!/usr/bin/env python3

def get_requirements():
    print("\nProgram Requirements:")
    print("1. Lists (Python data structure): mutable, ordered sequence of elements.")
    print("2. List are mutable/changeble--that is, can insert, update, delete.")
    print("3. Create list - using square brackets [lists]: my_list = [cherries, apples, bananas, oranges)].")
    print("4. Create a program that mirrors the following IPO (input/processing/output) format.")
    #print("4. Format, right-align numbers, and round to two decimal places")

def calculation():
    list_input = []

    list_input.append(input("Enter lsit element 1: "))
    list_input.append(input("Enter lsit element 2: "))
    list_input.append(input("Enter lsit element 3: "))
    list_input.append(input("Enter lsit element 4: "))

    print("\nOutput")
    print("Print my_list")
    print(list_input)

    insert_name = input("\nPlease enter list element: ")
    lck = False
    while lck == False:
        try:
            insert_numb = int(input("Please enter list *index* psoition (note: must convert to int): "))
            lck = True
        except ValueError:
            lck = False
            print("You have entered an inccorect value. Please enter a decimal number")
    
    list_input.insert(insert_numb, insert_name)

    print("\nInsert element into specific postion in my_list")
    print(list_input)

    print("\nCount number of elements in list:")
    print(len(list_input))

    print("\nSort elements in list alphabetically:")
    list_input.sort()
    print(list_input)

    print("\nReverse list:")
    list_input.sort(reverse = True)
    print(list_input)

    print("\nRemove last list element:")
    list_input.pop() #Removes from a list
    print(list_input)

    print("\nDeletes second element from list by *index* (note: 1-2n")
    del list_input[1] #Another way to remove elements
    print(list_input)

    print("\nDelete element from list by *value*")
    list_input.remove('cherries')
    print(list_input)

    print("\nRemove all the element:")
    list_input.clear()
    # or del my_list[:]
    print(list_input)