#!/usr/bin/env python3

#Constants
SQRFT_TO_ACRE = 1/43560

#Varibles
inp_sqr_feet = 0
total = 0

#Control Varibles
lck_1 = True #Used to control the input loop.

#Description
print("Program Requirements:")
print("1. Research: number of square deet to acre of land\n2. Must use float data type for user input and calculations\n3.Format and round conversion to two decimal places.")

#Collects the square feert
print("\nInput:")

while lck_1 == True:
    try:
        inp_sqr_feet = float(input("Enter square feet: "))
        lck_1 = False
    except ValueError:
        print("\nInvalid entry, please enter the square feet.\nOnly use numbers and/or decimals\nEx: 25000.50\n")
        lck_1 = True

#Computes the Square Feet
total = float(inp_sqr_feet * SQRFT_TO_ACRE)    

#Prints the resuts
print("\nOutput")
print('{:,.2f}'.format(inp_sqr_feet) + " square feet = " + '{:,.2f}'.format(round(total, 2)) + " acres.")