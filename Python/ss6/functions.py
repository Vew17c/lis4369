 #!/usr/bin/env python3

def get_requirements():
    print("\nProgram Requirements:")
    print("1. Print with loop.")
    print("2. Print for loop using range() function, and implicted and explict list.")
    print("3. Use break and continue statements")
    #print("4. Format, right-align numbers, and round to two decimal places")

def looping():
    list1 = [1, 2, 3]
    list2 = ["Michigan" , "Alabama" , "Florida"]

    lck = False #Keeps the loop in check
    test_n = 0

    print("\n1. While Loop")
    while test_n < len(list1):
        print(list1[test_n])
        test_n = test_n + 1

    print("\n2. For Loop using Range w 1 arg")
    for x in range(4):
        print(x)

    print("\n3. For Loop using Range w 2 args")
    for x in range(1,4):
        print(x)

    print("\n4. For Loop using Range w 3 args (Intervals)")
    for x in range(1, 4, 2):
        print(x)

    print("\n5. For Loop using Range w 3 args (Negative Intervals)")
    for x in range(3, 0, -2):
        print(x)

    print("\n6. For Loop using (implicted) list")
    for x in [1, 2, 3]:
        print(x)

    print("\n7. For loop iterating throug (explicted) string list")
    for x in range(len(list2)):
        print(list2[x])


    print("\n8. For loop using break statements")
    for listing in list2:
        if listing == "Florida":
            break
        print(listing)

    print("\n9. For loop using continue statements")
    for listing in list2:
        if listing == "Alabama":
            continue
        print(listing)

    print("\n10. print list length")
    print(len(list2))