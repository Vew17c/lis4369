#!/usr/bin/env python3
def get_requirements():
    print("\nProgram Requirements:")
    print("1. Program converts user-entered temperatures into Farenheit or Celsius scales.")
    print("2. Program continues to prompt for user entry until no longer requested")
    print("3. Note: upper or lower case letters permitted. Though, incorrect entries are not permitted.")
    print("4. Note: Program does not validate numeric data (option requirement).")

def calculation():
    lck = True
    lck_u = True

    print("Input: ")

    while (lck == True):
        confirm = input("Do you want ot convert a temperature (y/n)?: ")
        confirm = confirm.lower()
        if confirm == "y":
            lck = False
        elif confirm == "n":
            print("Thank you for using our Temperature Conversion Program!")
            lck = False
            lck_u = False
        else:
            print("Incorrect Value. Please try again.\n")
            lck = True

    if lck_u == True:
        print("\nOutput:")

    while (lck_u == True):
        lck1 = False
        scale = input("Fahrenheit to Celsius? Type 'f', or Celsius to Fahrenheit? Type 'c': ")
        if scale == "f":
            try:
                temp = float(input("Enter temperature in Fahrenheit: "))
                temp = (temp - 32) / (5/9)
                print("Temperature in Celsius = " , "{:,0.2f}".format(temp))
                print()
            except ValueError:
                print("Incorrect Value. Please try again.\n")
                lck1 = True
        elif scale == "c":
            try:
                temp = float(input("Enter temperature in Celsius: "))
                temp = (temp * (9/5)) + 32
                print("Temperature in Fahrenheit = " , "{:,0.2f}".format(temp))
                print()
            except ValueError:
                print("Incorrect Value. Please try again.\n")
                lck1 = True
        else:
            print("Incorrect Value. Please try again.\n")
            lck1 = True
            

        if lck1 == True:
            confirm = input("Do you want ot convert a temperature (y/n)?: ")
            confirm = confirm.lower()
            if confirm == "y":
                lck1 = False
                lck_u = True
            elif confirm == "n":
                print("Thank you for using our Temperature Conversion Program!")
                lck1 = False
                lck_u = False
            else:
                print("Incorrect Value. Please try again.\n")
                lck1 = True
        

        