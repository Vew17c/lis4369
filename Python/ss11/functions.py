#!/usr/bin/env python3
def get_requirements():
    print("\nProgram Requirements:")
    print("1. Get user eggining and ending integer values, and store in two varibles.")
    print("2. Display 10 psuedo-random numbers between and including above values.")
    print("3. Must use integer data types")
    print("4. Example 1: Using range() and randint() functions")
    print("5. Example 2: Using a list, with range () and shuffle() functions:")

def calculation():
    import random
    from random import randint

    print("Input: ")
    start = int(input("Enter beginning value: "))
    end = int(input("Enter ending value: "))

    print("\n\nOutput")
    print("Example 1: Using range() and randint() functions:")
    for count in range(10):
        print(random.randint(start, end), sep=", ", end="")
    print()

    print("Example 2: Using a list, with range () and shuffle() functions:")
    r = list(range(start, end + 1))
    random.shuffle(r)
    for i in r:
        print (i, sep=",", end =" ")

    print()