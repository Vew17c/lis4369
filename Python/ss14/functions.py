 #!/usr/bin/env python3

def get_requirements():
    print("\nProgram Requirements:")
    print("1. Use Python selection structure.")
    print("2. Prompt for two numbers, and a suitable operator.")
    print("3. Test for correct numeric operator.")
    #print("4. Format, right-align numbers, and round to two decimal places")

def calc_percentage():
    numb1 = 0
    numb2 = 0
    test_val = 0

    lck = False #Keeps the loop in cehck

    #Collects the input
    print("\nInput: ")

    while lck == False:
        try:
            numb1 = float(input("Enter num1: "))
            print()
            break
        except ValueError:
            print("\nYou have entered an inccorect value. Please enter a decimal number")
            continue
    
    while lck == False:
        try:
            numb2 = float(input("Enter num2: "))
            print()
            break
        except ValueError:
            print("\nYou have entered an inccorect value. Please enter a decimal number")
            continue

    print("\nSuitable Operators: +, -, *, / , // (integer division), % (modulo operator), ** (power) or POW")
    calc_operator (numb1, numb2)

def fix_denominator():
    lck = False
    while lck == False:
        try:
            numb = float(input("Enter num2: "))
            print()
            return numb
        except ValueError:
            print("\nYou have entered an inccorect value. Please enter a decimal number")
            continue

def calc_operator (numb1, numb2):
    import operator

    lck = False
    test_val = 0
    total = 0

    test_val = input("Enter operator: ").lower()
    print()

    while lck == False:
        try:
            ops = { #Modify this part if you are putting in a calculation that doenst have the ability to crash the program
                "+": numb1 + numb2, 
                "-": numb1 - numb2, 
                "*": numb1 * numb2,  
                "**": numb1 ** numb2, 
                "pow": pow(numb1, numb2)
                }  

            if test_val == "/":
                if numb2 == 0: 
                    print("You cant divide by zero")
                    numb2 = fix_denominator()
                else:
                    total = numb1 / numb2
                    print("{:,.2f}".format(total))
                    break
            elif test_val == "//":
                if numb2 == 0: 
                    print("You cant divide by zero")
                    numb2 = fix_denominator()
                else:
                    total = numb1 // numb2
                    print("{:,.2f}".format(total))
                    break
            elif test_val == "%":
                if numb2 == 0: 
                    print("You cant use modulo with a denomniator of zero")
                    numb2 = fix_denominator()
                else:
                    total = numb1 % numb2
                    print("{:,.2f}".format(total))
                    break
            else:
                total = ops[test_val]
                print("{:,.2f}".format(total))
                break    
        except KeyError:
            print("Invalid operator inputed!")  
            test_val = input("Enter operator: ").lower()
            print()
            continue

    print("Thank you for using our Math Calculator!")