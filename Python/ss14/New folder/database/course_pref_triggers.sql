-- Create "after insert on course_pref" trigger that automatically creates an audit record in the course_pref_history table.

-- %%%%% Create AFTER INSERT trigger using NEW keyword: %%%%%
drop trigger if exists trg_course_pref_after_insert;

-- temporarily redefine delimiter
delimiter //
create trigger trg_course_pref_after_insert
  AFTER INSERT on course_pref
  FOR EACH ROW
  BEGIN
  INSERT into course_pref_history 
	(crp_id, crh_id, inh_id, cph_pref, cph_delivery, cph_term, cph_day, cph_time_of_day, cph_start, cph_end, cph_room, cph_typical, cph_change, cph_notes)
  values
	(NEW.crp_id, NEW.crs_id, NEW.ins_id, NEW.crp_pref, NEW.crp_delivery, NEW.crp_term, NEW.crp_day, NEW.crp_time_of_day, NEW.crp_start, NEW.crp_end, NEW.crp_room, NEW.crp_typical, now(), concat(user(), " added a new course pref. ", NEW.crp_notes));
-- user(): returns user name and host name provided by client
  END //
delimiter ;

-- test tables before INSERT
select * from course_pref;
select * from course_pref_history;

-- test trigger (add new course_pref record here)...

-- test tables after INSERT
select * from course_pref;
select * from course_pref_history;
