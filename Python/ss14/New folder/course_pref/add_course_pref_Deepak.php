<?php include_once("../global/error_display.php"); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js" ></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
		<meta name="description" content="FSU's iSchool Instructor Course Preference System">
		<meta name="author" content="DIS Team: LIS4905_5900 Fall 2019">
		<link rel="icon" href="favicon.ico">
		<title>FSU's iSchool Instructor Course Preference System</title>
		
		<!--Include css file-->
	<?php include_once("../css/include_css.php");?>
</head>
<body>
	<?php include_once("../global/nav.php"); ?>
	<?php
	$hostname = "localhost:3308";
	$username = "mjowett";
	$password = "{faith}";
	$dbname = "instructor_pref";

	/*
	$hostname = "localhost";
	$username = "root";
	$password = "mysql";
	$dbname = "faculty_pref";
*/
	$conn = mysqli_connect($hostname, $username, $password,$dbname);
	if (!$conn){
		die('Could not connect: '.mysqli_connect_error());
	}
	mysqli_select_db($conn,$dbname);
	
	$sql = "SELECT  crs_num,crs_name FROM course";
	$result = mysqli_query($conn, $sql);
	$opt = "<select class='form-control' name='course'> <option> Select an option</option>";
	while($row = mysqli_fetch_assoc($result)){
		$opt .= "<option>{$row['crs_num']} {$row['crs_name']} </option>";
	}
	$opt .= "</select>";
	$secondsql = "SELECT ins_lname FROM instructor";
	$secondresult = mysqli_query($conn, $secondsql);
	$secondopt = "<select class='form-control' name='instructor'> <option> Select an option </option>";
	while($row = mysqli_fetch_assoc($secondresult)){
		$secondopt .= "<option>{$row['ins_lname']}</option>";
	}
	$secondopt .= "</select>";
	?>
	<div class="container">
		<div class="starter-template">
					<h2 style="text-align:center;">Course Preference</h2> <br>						
					<div class="container"> 
						<form id="defaultForm" method="post" class="form-horizontal" action="#">
								<div class="form-group">
										<label class="col-sm-4 control-label">Course:</label>
										<div class="col-sm-4">
												<?php echo $opt; ?>
										</div>
								</div>
								<div class="form-group">
										<label class="col-sm-4 control-label">Instructor:</label>
										<div class="col-sm-4">
												<?php echo $secondopt; ?>
										</div>
								</div>
								<div class="form-group">
										<label class="col-sm-4 control-label">Course Preference:</label>
										<div class="col-sm-4">
												<select class="form-control" name="course_preference"/>
												<option disabled selected value>Select an option</option>
												<option value="y">Yes</option>
												<option value="n">No</option>
												</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Course Preference delivery:</label>
										<div class="col-sm-4">
												<select class="form-control" name="crp_delivery" />
												<option disabled selected value>Select an option</option>
												<option value="y">Yes</option>
												<option value="n">No</option>
												</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Course Preference term:</label>
										<div class="col-sm-4">
												<select class="form-control" name="crp_term"/>
												<option disabled selected value>Select an option</option>
												<option value="y">Yes</option>
												<option value="n">No</option>
												</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Course Preference day:</label>
										<div class="col-sm-4">
												<select class="form-control" name="crp_day"/>
												<option disabled selected value>Select an option</option>
												<option value="y">Yes</option>
												<option value="n">No</option>
												</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Course Preference time of day:</label>
										<div class="col-sm-4">
												<select class="form-control" name="crp_time_of_day" />
												<option disabled selected value>Select an option</option>
												<option value="y">Yes</option>
												<option value="n">No</option>
												</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Course Preference start time:</label>
												<div class="col-sm-4">
													<div class='input-group date' id='datetimepicker3'>
														<input type='text' class="form-control" />
														<span class="input-group-addon">
															<span class="glyphicon glyphicon-time"></span>
														</span>
													</div>
												</div>	
													<script type="text/javascript">
												$(function () {
													$('#datetimepicker3').datetimepicker({
														format: 'HH:mm'
													});
												});
											</script>
												</div>
							
								<div class="form-group">
												<label class="col-sm-4 control-label">Course Preference end time:</label>
												<div class="col-sm-4">
													<div class='input-group date' id='datetimepicker4'>
														<input type='text' class="form-control" />
														<span class="input-group-addon">
															<span class="glyphicon glyphicon-time"></span>
														</span>
													</div>
												</div>	
													<script type="text/javascript">
												$(function () {
													$('#datetimepicker4').datetimepicker({
														format: 'HH:mm'
													});
												});
											</script>
												</div>
											
								<div class="form-group">
										<label class="col-sm-4 control-label">Course Preference room:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="crp_room" placeholder="Ex : WJB 2010"/>
										</div>
								</div>
								
								<div class="form-group">
										<label class="col-sm-4 control-label">Course Preference typical:</label>
										<div class="col-sm-4">
												<select class="form-control" name="crp_typical"/>
												<option disabled selected value>Select an option</option>
												<option value="y">Yes</option>
												<option value="n">No</option>
												</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Notes:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="notes" />
										</div>
								</div>

								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-5">
									<button type="submit" class="btn btn-primary" name="add" value="Add">Add</button>
										</div>
								</div>
						</form>	
		</div>
 </div> 
<script type="text/javascript">
 //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
 
 $(document).ready(function() {
	$('#defaultForm').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {

					crp_room: {
							validators: {
									notEmpty: {
											message: 'Course Preference Room is required'
									},
									stringLength: {
											min: 1,
											max: 10,
											message: 'Course Preference Room no more than 10 characters'
									},
									regexp: {
										regexp: /^[ a-zA-Z0-9]+$/,
									message: 'Course Preference room can only contain letters and numbers'
									},									
							},
					},
					notes: {
							validators: {
									stringLength: {
											min: 1,
											max: 255,
											message: 'Notes'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[\w\-\s]+$/,		
									message: 'Notes'
									},									
							},
					}
					
			}
	});
});
</script>
</body>
</html>
