<?php
require_once "../global/connection.php";
$query = "SELECT * FROM course ORDER BY crs_id";

$statement = $db->prepare($query);
$statement->execute();
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Mark K. Jowett, Ph.D.">
	<link rel="icon" href="favicon.ico">

		<title>DIS Project</title>
		<?php include_once("../css/include_css_data_tables.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>
	
	<div class="container-fluid">
		 <div class="starter-template">
						<div class="page-header">
							<?php include_once("../global/header.php"); ?>	
						</div>

						<h2>Course</h2>

<a href="add_course.php">Add Course</a>
<br />

 <div class="table-responsive">
	 <table id="myTable" class="table table-striped table-condensed"  >

		 <!-- Code displaying course data with Edit/Delete buttons goes here // -->
		 <thead>
		 <tr>
			<th style="text-align:center">Number</th>
			<th style="text-align:center">Name</th>
			<th style="text-align:center">Hours</th>
			<th style="text-align:center">Steward</th>
			<th style="text-align:center">Required</th>
			<th style="text-align:center">Degree</th>
			<th style="text-align:center">IEP</th>
			<th style="text-align:center">Accreditation</th>
			<th style="text-align:center">Delivery</th>
			<th style="text-align:center">Notes</th>
		 </tr>
		 </thead>

		 <?php
		 $result = $statement->fetch();
		 while($result != null)
		 {
			?>
			<tr>
			<td style="text-align:left"><?php echo htmlspecialchars($result['crs_num']); ?></td>
			<td style="text-align:left"><?php echo htmlspecialchars($result['crs_name']); ?></td>
			<td><?php echo htmlspecialchars($result['crs_credit_hrs']); ?></td>
			<td style="text-align:left"><?php echo htmlspecialchars($result['crs_steward']); ?></td>
			<td style="text-align:left"><?php echo htmlspecialchars($result['crs_required']); ?></td>
			<td style="text-align:left"><?php echo htmlspecialchars($result['crs_degree']); ?></td>
			<td><?php echo htmlspecialchars($result['crs_iep']); ?></td>
			<td><?php echo htmlspecialchars($result['crs_accreditation']); ?></td>
			<td style="text-align:left"><?php echo htmlspecialchars($result['crs_delivery']); ?></td>
			<td style="text-align:left"><?php echo htmlspecialchars($result['crs_notes']); ?><td>

			<td>
				<form
				onsubmit = "return confirm('Do you really want to delete record?');"
				action="delete_course.php"
				method="post"
				id="delete_course">

					<input type="hidden" name="crs_id" value="<?php echo $result['crs_id']; ?>" />
					<input type="submit" value="Delete" />
				</form>
			</td>

			<td>
				<form action="edit_course.php" method="post" id="edit_course">

					<input type="hidden" name="crs_id" value="<?php echo $result['crs_id']; ?>" />
					<input type="submit" value="Edit" />
				</form>
			</td>

			</tr>
			<?php	
				$result = $statement->fetch();
		 }
		 $statement->closeCursor();
		 $db = null;

		 ?>


	 </table>
 </div> <!-- end table-responsive -->
 	
<?php
include_once "../global/footer.php";
?>

			</div> <!-- end starter-template -->
  </div> <!-- end container -->

	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
		<?php include_once("../js/include_js_data_tables.php"); ?>	

		<script type="text/javascript">
	 $(document).ready(function(){
		 $('#myTable').DataTable({
			// "pageLength": 5
			 //https://datatables.net/reference/option/lengthMenu
			 //1st inner array page length values; 2nd inner array displayed options
			 //Note: -1 is used to disable pagination (i.e., display all rows)
			//Note: pageLength property automatically set to first value given in array
		 "lengthMenu": [[ 10, 25, 50, -1], [ 10, 25, 50, "All"]],
		 
	 //permit sorting (i.e., no sorting on last two columns: delete and edit)
    "columns":
		[
		null,			
		null,
		null,
		null,
		null,			
		null,
		null,
		null,
		null,			
		null,
		null,
     { "orderable": false },
     { "orderable": false }			
    ]
		 });
});
	</script>

</body>
</html>
