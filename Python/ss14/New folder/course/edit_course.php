<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Will Bollock!">
	<link rel="icon" href="../img/favicon.ico">

	<title>LIS4905 - Project</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("../global/header.php"); ?>	
					</div>

					<h2>Course</h2>

						<form id="defaultForm" method="post" class="form-horizontal" action="edit_course_process.php">

                        <?php
						require_once "../global/connection.php";
						require_once "../global/functions.php";

                        //capture's ID
                        $crs_id_v = $_POST['crs_id'];

                        //find all data associated with selected petstore ID above
                        $query = 
                        "SELECT * 
                        FROM course
                        WHERE crs_id = :crs_id_p";

                        $statement = $db->prepare($query);
                        $statement->bindParam(':crs_id_p', $crs_id_v);
                        $statement->execute();
                        $result = $statement->fetch();
                        while($result != null)
                        {
                            ?>
                                <input type="hidden" name="crs_id" value="<?php echo $result['crs_id']; ?>" />

								<div class="form-group">
										<label class="col-sm-4 control-label">Number:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="number" value="<?php echo htmlspecialchars($result['crs_num']); ?>"/>
										</div>
								</div>
								
								
                                
                                <div class="form-group">
										<label class="col-sm-4 control-label">Name:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="name" value="<?php echo htmlspecialchars($result['crs_name']); ?>"/>
										</div>
								</div>


								<?php
								//call UDF (user-defined function)
								$sql = "SELECT crs_id, crs_credit_hrs FROM course";								
								$results = getResultSet($sql);
								//print_r($results);  //for testing
								//exit();
								?>

								<div class="form-group">
										<label class="col-sm-4 control-label">Hours:</label>
										<div class="col-sm-4">
										<select class="form-control" name="credits">
											<option value="3" <?php if ($result['crs_credit_hrs'] == "3") { echo 'selected'; } ?>>3</option>
											<option value="2" <?php if ($result['crs_credit_hrs'] == "2") { echo 'selected'; } ?>>2</option>
											<option value="1" <?php if ($result['crs_credit_hrs'] == "1") { echo 'selected'; } ?>>1</option>
											</select>
										</div>
								</div>
								
								<?php
								//call UDF (user-defined function)
								$sql = "SELECT ins_id, ins_fname, ins_lname FROM instructor ORDER BY ins_lname";									
								$results = getResultSet($sql);
								//print_r($results);  //for testing
								//exit();
								
								?>
								<div class="form-group">
										<label class="col-sm-4 control-label">Steward:</label>
										<div class="col-sm-4">
										<select class='form-control' name="steward" >
											<!-- option>Make selection...</option -->
											<?php
											$loop_v = 1;
											foreach($results as $result1) :
											?>
												<option value="<?php echo $result1['ins_lname'].', '.$result1['ins_fname']; ?>" <?php if ($result1['ins_lname'].', '.$result1['ins_fname'] == $result['crs_steward']) {echo 'selected';} $loop_v += 1; ?>>
													<?php echo htmlspecialchars($result1['ins_lname'].', '.$result1['ins_fname']); ?>
													<!--NOTE: Steward must be entered correctly through add course to edit properly -->
													
												</option>
											<?php
											endforeach;
											?>
										</select>
										</div>
								</div>
                                
                                <div class="form-group">
										<label class="col-sm-4 control-label">Required:</label>
										<div class="col-sm-4">
										<select class="form-control" name="required">
											<option value="y" <?php if ($result['crs_required'] == "y") { echo 'selected'; } ?>>Yes</option>
											<option value="n" <?php if ($result['crs_required'] == "n") { echo 'selected'; } ?>>No</option>
											<option value="u" <?php if ($result['crs_required'] == "u") { echo 'selected'; } ?>>Undecided</option>
											</select>
										</div>
                                </div>


								<?php
								//call UDF (user-defined function)
								$sql = "SELECT prg_id, prg_name FROM program";								
								$results = getResultSet($sql);
								//print_r($results);  //for testing
								//exit();
								?>

                                <div class="form-group">
										<label class="col-sm-4 control-label">Program:</label>
										<div class="col-sm-4">
										<select class="form-control" name="degree[]" multiple>
										<?php
											$loop_v = 1;
											// give us an array of degree values
											$degree_array = explode(",", $result['crs_degree']);
												// loop through *program* table
												foreach($results as $result1) :
													
												?>
													<!-- actually put the prg names in the edit field -->
													<option value="<?php echo $result1['prg_name']; ?>" 
													<?php 
														for ($i = 0; $i < sizeof($degree_array); $i++)
														{
															if ($degree_array[$i] == $result1['prg_name'])
															{ echo 'selected'; }
														}
															?>
												<?php
												$loop_v += 1;
												 ?>>
													<?php echo htmlspecialchars($result1['prg_name']); ?>
													
													
												</option>
											<?php
											endforeach;
											?>
											</select>
											
											<!-- super happy this works. the magic is crs_degre == prg_name. $result refers to the current entity/user, loops through prg names, stops when it 
													found a match -->
											
										<!-- Leaving code for legacy... pulling from prg instead
											<option value="Pathway" <?php if ($result['crs_degree'] == "Pathway") { echo 'selected'; } ?>>BSIT/MSIT Pathway</option>
											<option value="GCHIT" <?php if ($result['crs_degree'] == "GCHIT") { echo 'selected'; } ?>>Graduate Certificate in Health Information Technology</option>
											<option value="UCHIT" <?php if ($result['crs_degree'] == "UCHIT") { echo 'selected'; } ?>>Undergraduate Certificate in Health Information Technology</option>
											<option value="GCIA" <?php if ($result['crs_degree'] == "GCIA") { echo 'selected'; } ?>>Graduate Certificate in Information Architecture</option>
											<option value="GCILM" <?php if ($result['crs_degree'] == "GCILM") { echo 'selected'; } ?>>Graduate Certificate in Information Leadership & Management</option>
											<option value="UPIT" <?php if ($result['crs_degree'] == "UPIT") { echo 'selected'; } ?>>Undergraduate Program in Information Technology</option>
											<option value="UPICT" <?php if ($result['crs_degree'] == "UPICT") { echo 'selected'; } ?>>Undergraduate Program in Information, Communication, and Technology</option>
											<option value="MSI" <?php if ($result['crs_degree'] == "MSI") { echo 'selected'; } ?>>Master of Science in Information</option>
											<option value="MSIT" <?php if ($result['crs_degree'] == "MSIT") { echo 'selected'; } ?>>Master of Science in Information Technology</option>
											<option value="MIT" <?php if ($result['crs_degree'] == "MIT") { echo 'selected'; } ?>>Minor in Information Technology</option>
											<option value="PHDI" <?php if ($result['crs_degree'] == "PHDI") { echo 'selected'; } ?>>Ph.D. in Information</option>
											<option value="GCRS" <?php if ($result['crs_degree'] == "GCRS") { echo 'selected'; } ?>>Graduate Certificate in Reference Services</option>
											<option value="GCSLL" <?php if ($result['crs_degree'] == "GCSLL") { echo 'selected'; } ?>>Graduate Certificate in School Librarian Leadership</option>
											<option value="SI" <?php if ($result['crs_degree'] == "SI") { echo 'selected'; } ?>>Specialist in Information</option>
											<option value="GCYS" <?php if ($result['crs_degree'] == "GCYS") { echo 'selected'; } ?>>Graduate Certificate in Youth Services</option>
											-->
											<!--NOTE: Only data that is entered with the new course.php will work. See values.. data must match -->
											
										</div>
                                </div>

                                <div class="form-group">
										<label class="col-sm-4 control-label">IEP:</label>
										<div class="col-sm-4">
										<select class="form-control" name="iep">
											<option value="y" <?php if ($result['crs_iep'] == "y") { echo 'selected'; } ?>>Yes</option>
											<option value="n" <?php if ($result['crs_iep'] == "n") { echo 'selected'; } ?>>No</option>
											<option value="u" <?php if ($result['crs_iep'] == "u") { echo 'selected'; } ?>>Undecided</option>
											</select>
										</div>
                                </div>

                                <div class="form-group">
										<label class="col-sm-4 control-label">Accreditation:</label>
										<div class="col-sm-4">
										<select class="form-control" name="accreditation">
											<option value="y" <?php if ($result['crs_accreditation'] == "y") { echo 'selected'; } ?>>Yes</option>
											<option value="n" <?php if ($result['crs_accreditation'] == "n") { echo 'selected'; } ?>>No</option>
											<option value="u" <?php if ($result['crs_accreditation'] == "u") { echo 'selected'; } ?>>Undecided</option>
											</select>
										</div>
                                </div>

                                <div class="form-group">
										<label class="col-sm-4 control-label">Delivery:</label>
										<div class="col-sm-4">
										<select class="form-control" name="delivery">
											<option value="online" <?php if ($result['crs_delivery'] == "online") { echo 'selected'; } ?>>Online</option>
											<option value="f2f" <?php if ($result['crs_delivery'] == "f2f") { echo 'selected'; } ?>>Face To Face</option>
											<option value="both" <?php if ($result['crs_delivery'] == "both") { echo 'selected'; } ?>>Both</option>
											</select>
										</div>
                                </div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Notes:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="notes" value="<?php echo htmlspecialchars($result['crs_notes']); ?>"/>
										</div>
								</div>

                                <?php
                                $result = $statement->fetch();
                                }
                                $db = null;
                                ?>


								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-3">
									<button type="submit" class="btn btn-primary" name="edit" value="Edit">Update</button>
										</div>
								</div>
						</form>

			<?php include_once "../global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("../js/include_js.php"); ?>

			<script type="text/javascript">
 //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
 $(document).ready(function() {

	$('#defaultForm').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {
					number: {
							validators: {
									notEmpty: {
									 message: 'Course number required'
									},
									stringLength: {
											min: 1,
											max: 30,
									 message: 'Course number no more than 30 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[\w\-\s]+$/,
										message: 'Course number can only contain letters, numbers, hyphens, and underscore'
									},									
							},
					},

					name: {
							validators: {
									notEmpty: {
									 message: 'Course name required'
									},
									stringLength: {
											min: 1,
											max: 100,
									 message: 'Course name no more than 100 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[\w\-\s]+$/,
										message: 'Name can only contain letters, numbers, hyphens, and underscore'
									},									
							},
					},

					credits: {
							validators: {
									notEmpty: {
											message: 'Course Credit Hours is required'
									},
									stringLength: {
											min: 1,
											max: 1,
											message: 'Course Credit Hours must be 1 number'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[0-9]+$/,		
									message: 'Course Credit Hours must have one number'
									},									
							},
					},

					steward: {
							validators: {
									notEmpty: {
									 message: 'Course Steward required'
									},
									stringLength: {
											min: 1,
											max: 30,
									 message: 'Course Steward is no more than 30 characters'
									},
									regexp: {
										regexp: /^[\w\-\s,]+$/,		
									message: 'Course Steward can only contain letters'
									},								
							},
					},

					required: {
							validators: {
									notEmpty: {
									 message: 'Course Required is required'
									},
									stringLength: {
											min: 1,
											max: 10,
									 message: 'Course Required is no more than 1 characters'
									},
									regexp: {
										regexp: /^[\w\-\s]+$/,		
									message: 'Course Steward can only contain letters'
									},								
							},
					},

					degree: {
							validators: {
									notEmpty: {
									 message: 'Course Program is required'
									},
									stringLength: {
											min: 1,
											max: 45,
									 message: 'Course Program is no more than 45 characters'
									},
									regexp: {
										regexp: /^[\w\-\s.,/]+$/,		
									message: 'Course Program can only contain letters'
									},								
							},
					},

					iep: {
							validators: {
									notEmpty: {
									 message: 'IEP is required'
									},
									stringLength: {
											min: 1,
											max: 1,
									 message: 'IEP is no more than 1 character'
									},
									regexp: {
										regexp: /^[\w\-\s]+$/,		
									message: 'IEP can only contain letters'
									},								
							},
					},

					accreditation: {
							validators: {
									notEmpty: {
									 message: 'Accreditation is required'
									},
									stringLength: {
											min: 1,
											max: 1,
									 message: 'Accreditation is no more than 1 character'
									},
									regexp: {
										regexp: /^[\w\-\s]+$/,		
									message: 'Accreditation can only contain letters'
									},								
							},
					},

					delivery: {
							validators: {
									notEmpty: {
									 message: 'Delivery is required'
									},
									stringLength: {
											min: 1,
											max: 10,
									 message: 'Delivery is no more than 10 characters'
									},
									regexp: {
										regexp: /^[\w\-\s]+$/,		
									message: 'Delivery can only contain letters'
									},								
							},
					},

					notes: {
							validators: {
									stringLength: {
											min: 1,
											max: 255,
											message: 'Notes'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[\w\-\s]+$/,		
									message: 'Notes'
									},									
							},
					}
					
			}
	});
});
</script>

</body>
</html>
