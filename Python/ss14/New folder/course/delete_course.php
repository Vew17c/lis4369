<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//Get item ID
$crs_id_v = $_POST['crs_id'];
//exit($crs_id_v);

require_once('../global/connection.php');

//Delete item from database
$query = 
"DELETE FROM course
WHERE crs_id = :crs_id_p";

try 
{
    $statement = $db->prepare($query);
    $statement->bindParam(':crs_id_p', $crs_id_v);
    $row_count = $statement->execute();
    $statement->closeCursor();

    header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)
}

catch(PDOException $e)
{
    $error = $e->getMessage();
    echo $error;
}

?>