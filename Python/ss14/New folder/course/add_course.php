<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Will Bollock was here :)">
	<link rel="icon" href="../img/favicon.ico">

	<title>LIS4905</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>
	<?php include_once("../global/functions.php");?>
	<?php include_once("../global/connection.php");?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("../global/header.php"); ?>	
					</div>

					<h2>Course</h2>

						<form id="defaultForm" method="post" class="form-horizontal" action="add_course_process.php">

						<?php
								// borrowed from mjowett :)
								//call UDF (user-defined function)
								$sql = "SELECT crs_id, crs_num, crs_name, crs_credit_hrs, crs_steward, crs_required, crs_degree, crs_iep, crs_accreditation, crs_delivery, crs_notes FROM course ORDER BY crs_num";								
								$results = getResultSet($sql);
								//print_r($results);  //for testing
								//exit();
								?>
								<div class="form-group">
										<label class="col-sm-4 control-label">Number:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="number" placeholder="Ex: COP2258"/>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Name:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="name" placeholder="Ex: Database Concepts"/>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Hours:</label>
										<div class="col-sm-4">
										<select class="form-control" name="credits">
												<option value="1" selected="selected">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
											</select>
										</div>
								</div>
									<?php
									//call UDF (user-defined function)
									$sql = "SELECT ins_id, ins_fname, ins_lname FROM instructor ORDER BY ins_lname";								
									$results = getResultSet($sql);
									//print_r($results);  //for testing
									//exit();
									?>
								<div class="form-group">
										<label class="col-sm-4 control-label">Steward:</label>
										<div class="col-sm-4">
										<select class='form-control' name="steward" >
													<option>Make selection...</option>
													<?php
													foreach($results as $result) :
													?>
													<option value="<?php echo $result['ins_lname'].', '.$result['ins_fname'] ; ?>">
														<?php echo htmlspecialchars($result['ins_lname'].', '.$result['ins_fname']); ?>
													</option>
												<?php
												endforeach;
												?>
											</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Required:</label>
										<div class="col-sm-4">
										<select class="form-control" name="required">
												<option value="y" selected="selected">Yes</option>
												<option value="n">No</option>
												<option value="u">Undecided</option>
											</select>
										</div>
								</div>

								<?php
								// borrowed from mjowett :)
								//call UDF (user-defined function)
								$sql = "SELECT crs_id, crs_num, crs_name, crs_credit_hrs, crs_steward, crs_required, crs_degree, crs_iep, crs_accreditation, crs_delivery, crs_notes FROM course ORDER BY crs_num";								
								$results = getResultSet($sql);
								//print_r($results);  //for testing
								//exit();
								?>

								<?php
								//call UDF (user-defined function)
								$sql = "SELECT prg_id, prg_name FROM program";								
								$results = getResultSet($sql);
								//print_r($results);  //for testing
								//exit();
								?>
								<div class="form-group">
										<label class="col-sm-4 control-label">Program:</label>
										<div class="col-sm-4">
													<!-- [] in degree for array -->
													<select class='form-control' name="degree[]" multiple  >
													<?php foreach ($results as $row) : ?>
													<option><?php echo $row["prg_name"]?></option>
													<?php endforeach; ?>
											</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">IEP:</label>
										<div class="col-sm-4">
										<select class="form-control" name="iep">
												<option value="y" selected="selected">Yes</option>
												<option value="n">No</option>
												<option value="u">Undecided</option>
											</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Accreditation:</label>
										<div class="col-sm-4">
										<select class="form-control" name="accreditation">
												<option value="y" selected="selected">Yes</option>
												<option value="n">No</option>
												<option value="u">Undecided</option>
											</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Delivery:</label>
										<div class="col-sm-4">
										<select class="form-control" name="delivery">
												<option value="online" selected="selected">Online</option>
												<option value="f2f">Face to Face</option>
												<option value="both">Both</option>
												
											</select>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Notes:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="notes" />
										</div>
								</div>

								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-3">
									<button type="submit" class="btn btn-primary" name="add" value="Add">Add</button>
										</div>
								</div>
						</form>

			<?php include_once "../global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("../js/include_js.php"); ?>

<script type="text/javascript">
 //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
 $(document).ready(function() {

	$('#defaultForm').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {
					number: {
							validators: {
									notEmpty: {
									 message: 'Course number required'
									},
									stringLength: {
											min: 1,
											max: 30,
									 message: 'Course number no more than 30 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[\w\-\s]+$/,
										message: 'Course number can only contain letters, numbers, hyphens, and underscore'
									},									
							},
					},

					name: {
							validators: {
									notEmpty: {
									 message: 'Course name required'
									},
									stringLength: {
											min: 1,
											max: 100,
									 message: 'Course name no more than 100 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[\w\-\s]+$/,
										message: 'Name can only contain letters, numbers, hyphens, and underscore'
									},									
							},
					},

					credits: {
							validators: {
									notEmpty: {
											message: 'Course Credit Hours is required'
									},
									stringLength: {
											min: 1,
											max: 1,
											message: 'Course Credit Hours must be 1 number'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[0-9]+$/,		
									message: 'Course Credit Hours must have one number'
									},									
							},
					},

					steward: {
							validators: {
									notEmpty: {
									 message: 'Course Steward required'
									},
									stringLength: {
											min: 1,
											max: 30,
									 message: 'Course Steward is no more than 30 characters'
									},
									regexp: {
										regexp: /^[\w\-\s,]+$/,		
									message: 'Course Steward can only contain letters'
									},								
							},
					},

					required: {
							validators: {
									notEmpty: {
									 message: 'Course Required is required'
									},
									stringLength: {
											min: 1,
											max: 10,
									 message: 'Course Required is no more than 1 characters'
									},
									regexp: {
										regexp: /^[\w\-\s]+$/,		
									message: 'Course Steward can only contain letters'
									},								
							},
					},

					degree: {
							validators: {
									notEmpty: {
									 message: 'Course Program is required'
									},
									stringLength: {
											min: 1,
											max: 45,
									 message: 'Course Program is no more than 45 characters'
									},
									regexp: {
										regexp: /^[\w\-\s.,/]+$/,		
									message: 'Course Program can only contain letters'
									},								
							},
					},

					iep: {
							validators: {
									notEmpty: {
									 message: 'IEP is required'
									},
									stringLength: {
											min: 1,
											max: 1,
									 message: 'IEP is no more than 1 character'
									},
									regexp: {
										regexp: /^[\w\-\s]+$/,		
									message: 'IEP can only contain letters'
									},								
							},
					},

					accreditation: {
							validators: {
									notEmpty: {
									 message: 'Accreditation is required'
									},
									stringLength: {
											min: 1,
											max: 1,
									 message: 'Accreditation is no more than 1 character'
									},
									regexp: {
										regexp: /^[\w\-\s]+$/,		
									message: 'Accreditation can only contain letters'
									},								
							},
					},

					delivery: {
							validators: {
									notEmpty: {
									 message: 'Delivery is required'
									},
									stringLength: {
											min: 1,
											max: 10,
									 message: 'Delivery is no more than 10 characters'
									},
									regexp: {
										regexp: /^[\w\-\s]+$/,		
									message: 'Delivery can only contain letters'
									},								
							},
					},

					notes: {
							validators: {
									stringLength: {
											min: 1,
											max: 255,
											message: 'Notes'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: '/*/',		
									message: 'Notes can take almost any character'
									},									
							},
					}
					
			}
	});
});
</script>

</body>
</html>
