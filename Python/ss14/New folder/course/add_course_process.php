<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);
//exit(print_r($_POST)); //display $_POST array values captured from form 
// or, for nicer display in browser... 

/*echo "<pre>"; 

print_r($_POST); 
echo "</pre>"; 
exit(); //stop processing, otherwise, errors below 

*/
/*
Output below of what it passes through
Array
(
    [number] => COP2258
    [name] => William Bollock
    [credits] => 2
    [steward] => 2
    [required] => n
    [degree] => 3
    [iep] => y
    [accreditation] => y
    [delivery] => online
    [notes] => 
    [add] => Add
)
*/
require_once('../global/connection.php');
require_once("../global/functions.php");
//use for inital test of form inputs
//exit(print_r($_POST));
function trim_value(&$value)
{
	$value = trim($value); //removes whitespace and related characters from the beginning and end of string
}
// need to get array of degree IDs
// for each degree in array, find the ID and assign it to another array
$i = 0;
#echo "<pre>";
#echo(gettype($_POST['degree']));
$prg_id_array = array();
foreach ($_POST['degree'] as $degree) {

    $query = "SELECT prg_id FROM program where prg_name = "."'".$degree."'".";";
    #echo($query);
    $prg_id_array[$i] = getResultSet($query);
    # for some goddamn reason i get this: Array ( [0] => Array ( [0] => Array
    #echo("<br>");
    $i++;
    #echo($i);
    
}
# array of prg ids
/*
echo("ADD_COURSE_PROCESS");
# for some goddamn reason i get this: Array ( [0] => Array ( [0] => Array

print_r($prg_id_array);
echo "</pre>";
exit();
*/

$crs_degree_v = implode(",", $_POST['degree']);



//Iterates over each value in array passing them to the callback function



#array_filter($_POST, 'trim_value');
//Get item data
//No need for pst_id when adding, uses auto incriment
$crs_num_v = $_POST['number'];
$crs_name_v = $_POST['name'];
$crs_credit_hrs_v = $_POST['credits'];
$crs_steward_v = $_POST['steward'];
$crs_required_v = $_POST['required'];
#$crs_degree_v = $_POST['degree'];

#echo $crs_degree_v;
#$crs_degree_v = array();
#array_push($crs_degree_v,$_POST['degree']);
#$crs_degree_v_array = serialize($crs_degree_v);


$crs_accreditation_v = $_POST['accreditation'];
$crs_delivery_v = $_POST['delivery'];
$crs_notes_v = $_POST['notes'];


if($_POST['credits']=='1') 
{
	$crs_credit_hrs_v="1";
}
else if($_POST['credits']=='2')
{
	$crs_credit_hrs_v="2";
}
else
{
	$crs_credit_hrs_v="3";
}

if($_POST['required']=='y') 
{
	$crs_required_v="y";
}

else if($_POST['required']=='n')
{
	$crs_required_v="n";
}
else
{
	$crs_required_v="n";
}

if($_POST['iep']=='y') 
{
	$crs_iep_v="y";
}

else if($_POST['iep']=='n')
{
	$crs_iep_v="n";
}
else
{
	$crs_iep_v="u";
}

if($_POST['accreditation']=='y') 
{
	$crs_accreditation_v="y";
}

else if($_POST['accreditation']=='n')
{
	$crs_accreditation_v="n";
}
else
{
	$crs_accreditation_v="u";
}

if($_POST['delivery']=='online') 
{
	$crs_delivery_v="online";
}

else if($_POST['delivery']=='f2f')
{
	$crs_delivery_v="f2f";
}
else
{
	$crs_accreditation_v="u";
}



if(isset($_POST['crp_notes'])==true) 
{
	$crp_notes_v = $_POST['crp_notes'];
}
else
{
	$crp_notes_v = "";
}
//exit($pst_name_v.",".$pst_city_v);

//Validation for Server side
//Name
$pattern='/^[\w\-\s]+$/';
$valid_num = preg_match($pattern, $crs_num_v);
//lname
$pattern='/^[a-zA-Z\-\s]+$/';
$valid_name = preg_match($pattern, $crs_name_v);

$pattern='/^[0-9]+$/';
$valid_credit_hrs = preg_match($pattern, $crs_credit_hrs_v);

$pattern='/^[a-zA-Z\s]+$/';
$valid_steward = preg_match($pattern, $crs_steward_v);

$pattern='/^[a-zA-Z\s]+$/';
$valid_required = preg_match($pattern, $crs_required_v);


$pattern='/^[a-zA-Z\s,]+$/';
$valid_degree = preg_match($pattern, $crs_degree_v);

$pattern='/^[a-zA-Z\s]+$/';
$valid_iep = preg_match($pattern, $crs_iep_v);

$pattern='/^[a-zA-Z\s]+$/';
$valid_accreditation = preg_match($pattern, $crs_accreditation_v);

$pattern='/^[a-zA-Z\s]+$/';
$valid_delivery = preg_match($pattern, $crs_delivery_v);


//echo $valid_name, $valid_city, $valid_email, $valid_phone, $valid_state, $valid_street, $valid_url, $valid_ytdsales, $valid_zip;
//exit();

if (
    empty($crs_num_v) ||
    empty($crs_name_v) ||
    empty($crs_credit_hrs_v) ||
    empty($crs_steward_v) ||
    empty($crs_required_v) ||
    empty($crs_degree_v) ||
    empty($crs_iep_v) ||
    empty($crs_accreditation_v) ||
    empty($crs_delivery_v)
){
    $error = "All fields require data, except <b>Notes</b>. Check all fields and try again.";
    include('../global/error.php');
} 
else if ($valid_num === false){
    echo "Error in the pattern!";
}
else if ($valid_name === false){
    echo "Error in the pattern!";
}
else if ($valid_credit_hrs === false){
    echo "Error in the pattern!";
}
else if ($valid_steward === false){
    echo "Error in the pattern!";
}
else if ($valid_required === false){
    echo "Error in the pattern!";
}
else if ($valid_degree === false){
  echo "Error in the pattern!";
}
else if ($valid_iep  === false){
    echo "Error in the pattern!";
}
else if ($valid_accreditation === false){
    echo "Error in the pattern!";
}
else if ($valid_delivery === false){
    echo "Error in the pattern!";
} else {
    

#echo "BOTTOM OF ADD CP";
#BSIT/MSIT Comb Path,Grad Cert HIT,UG Cert in IA,Grad Cert ILM
#print_r($crs_degree_v);

addCourse
(
    $crs_num_v,
	$crs_name_v,
	$crs_credit_hrs_v,
	$crs_steward_v,
	$crs_required_v,
	$crs_degree_v,
	$crs_iep_v,
	$crs_accreditation_v,
	$crs_delivery_v,
    $crs_notes_v,
    $prg_id_array
);

}

?>
