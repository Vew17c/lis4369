<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));

//Get item data
//No need for pst_id when adding, uses auto incriment

$crs_id_v = $_POST['crs_id'];
$crs_num_v = $_POST['number'];
$crs_name_v = $_POST['name'];
$crs_credit_hrs_v = $_POST['credits'];
$crs_steward_v = $_POST['steward'];
$crs_required_v = $_POST['required'];
#crs_degree_v is an array from the edit page
$crs_degree_v = implode(",", $_POST['degree']);
$crs_iep_v = $_POST['iep'];
$crs_accreditation_v = $_POST['accreditation'];
$crs_delivery_v = $_POST['delivery'];
$crs_notes_v = $_POST['notes'];

//exit($crs_name_v.",".$crs_id_v);

//Validation for Server side
$pattern='/^[\w\-\s]+$/';
$valid_num = preg_match($pattern, $crs_num_v);

$pattern='/^[a-zA-Z\-\s]+$/';
$valid_name = preg_match($pattern, $crs_name_v);

$pattern='/^[0-9]+$/';
$valid_credit_hrs = preg_match($pattern, $crs_credit_hrs_v);

$pattern='/^[a-zA-Z\s]+$/';
$valid_steward = preg_match($pattern, $crs_steward_v);

$pattern='/^[a-zA-Z\s]+$/';
$valid_required = preg_match($pattern, $crs_required_v);


#$pattern='/^[a-zA-Z\s]+$/';
#$valid_degree = preg_match($pattern, $crs_degree_v);

$pattern='/^[a-zA-Z\s]+$/';
$valid_iep = preg_match($pattern, $crs_iep_v);

$pattern='/^[a-zA-Z\s]+$/';
$valid_accreditation = preg_match($pattern, $crs_accreditation_v);

$pattern='/^[a-zA-Z\s]+$/';
$valid_delivery = preg_match($pattern, $crs_delivery_v);


//echo $valid_name, $valid_city, $valid_email, $valid_phone, $valid_state, $valid_street, $valid_url, $valid_ytdsales, $valid_zip;
//exit();

if (
    empty($crs_num_v) ||
    empty($crs_name_v) ||
    empty($crs_credit_hrs_v) ||
    empty($crs_steward_v) ||
    empty($crs_required_v) ||
    #empty($crs_degree_v) ||
    empty($crs_iep_v) ||
    empty($crs_accreditation_v) ||
    empty($crs_delivery_v)
){
    $error = "All fields require data, except <b>Notes</b>. Check all fields and try again.";
    include('../global/error.php');
} 
else if ($valid_num === false){
    echo "Error in the course number!"; include('../global/error.php');
}
else if ($valid_name === false){
    echo "Error in the course name!"; include('../global/error.php');
}
else if ($valid_credit_hrs === false){
    echo "Error in the course credit hours!"; include('../global/error.php');
}
else if ($valid_steward === false){
    echo "Error in the course steward!"; include('../global/error.php');
}
else if ($valid_required === false){
    echo "Error in the course required!"; include('../global/error.php');
}
#else if ($valid_degree === false){
#    echo "Error in the course degree!"; include('../global/error.php');
#}
else if ($valid_iep  === false){
    echo "Error in the course iep!"; include('../global/error.php');
}
else if ($valid_accreditation === false){
    echo "Error in the course accreditation!"; include('../global/error.php');
}
else if ($valid_delivery === false){
    echo "Error in the course delievery!"; include('../global/error.php');
} else {

require_once('../global/connection.php');
require_once("../global/functions.php");


$i = 0;
$prg_id_array = array();
foreach ($_POST['degree'] as $degree) {

    $query = "SELECT prg_id FROM program where prg_name = "."'".$degree."'".";";
    #echo($query);
    $prg_id_array[$i] = getResultSet($query);
    # for some goddamn reason i get this: Array ( [0] => Array ( [0] => Array
    #echo("<br>");
    $i++;
    #echo($i);
    
}

$query = 
"UPDATE course
 SET
   crs_num = :crs_num_p,
   crs_name = :crs_name_p, 
   crs_credit_hrs = :crs_credit_hrs_p, 
   crs_steward = :crs_steward_p, 
   crs_required = :crs_required_p, 
   crs_degree = :crs_degree_p, 
   crs_iep = :crs_iep_p, 
   crs_accreditation = :crs_accreditation_p, 
   crs_delivery = :crs_delivery_p,
   crs_notes = :crs_notes_p
  WHERE crs_id = :crs_id_p";

//exit($query);

try
    {
    $statement = $db->prepare($query);
    $statement->bindParam(':crs_id_p', $crs_id_v);
    $statement->bindParam(':crs_num_p', $crs_num_v);
    $statement->bindParam(':crs_name_p', $crs_name_v);
    $statement->bindParam(':crs_credit_hrs_p', $crs_credit_hrs_v);
    $statement->bindParam(':crs_steward_p', $crs_steward_v);
    $statement->bindParam(':crs_required_p', $crs_required_v);
    $statement->bindParam(':crs_degree_p', $crs_degree_v);
    $statement->bindParam(':crs_iep_p', $crs_iep_v);
    $statement->bindParam(':crs_accreditation_p', $crs_accreditation_v);
    $statement->bindParam(':crs_delivery_p', $crs_delivery_v);
    $statement->bindParam(':crs_notes_p', $crs_notes_v);
    $statement->execute();
    $statement->closeCursor();

    $last_auto_increment_id = $db->lastInsertId();
    editProgramID($crs_id_v, $prg_id_array);
    //include('index.php'); //forwarding is faster, one trip to server
    header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)
    }
    catch (PDOException $e)
    {
        $error = $e->getMessage();
        echo $error;
    }
}
?>
