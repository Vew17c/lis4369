<?php
//display errors--*ONLY* while testing, after comment out!
require_once "../global/error_display.php";

//Trim spaces (Note: really only necessary with open field inputs. For example: first name, etc.)
function trim_value(&$value)
{
	$value = trim($value); //removes whitespace and related characters from the beginning and end of string
}
//Iterates over each value in array passing them to the callback function
array_filter($_POST, 'trim_value');

//Get each item data
$ins_id_v = $_POST['ins_id'];
$crs_id_v = $_POST['crs_id'];

if($_POST['crp_pref']=='y') 
{
	$crp_pref_v="y";
}
else if($_POST['crp_pref']=='n')
{
	$crp_pref_v="n";
}
else
{
	$crp_pref_v="u";
}

if($_POST['crp_delivery']=='f2f') 
{
	$crp_delivery_v="f2f";
}

else if($_POST['crp_delivery']=='online')
{
	$crp_delivery_v="online";
}
else
{
	$crp_delivery_v="both";
}

if(isset($_POST['crp_notes'])==true) 
{
	$crp_notes_v = $_POST['crp_notes'];
}
else
{
	$crp_notes_v = "";
}

//See PHP Regular Expressions:
//http://regexone.com/
//cheat sheet: http://www.rexegg.com/regex-quickstart.html

//ins_id: (preg_grep) returns array of elements of input that match pattern, that is, containing 1-5 digits (required)
$pattern='/^[\d{1,5}]+$/';
$valid_ins_id_v = preg_match($pattern, $ins_id_v);
//echo $valid_ins_id_v; //test output: should be 1 (i.e., valid)

//crs_id: must include 1-5 digits
$pattern='/^[\d{1,5}]+$/';
$valid_crs_id_v = preg_match($pattern, $crs_id_v);
//exit(print_r($valid_crs_id_v)); //test output: should be 1 (i.e., valid)

//pay: match only one of 3 values: y/n/u
$pattern='/^[ynu]+$/';
$valid_crp_pref_v = preg_match($pattern, $crp_pref_v);

//offer: match only one of 3 values: f2f|online|both
$pattern='/^(f2f|online|both)+$/';
$valid_crp_delivery_v = preg_match($pattern, $crp_delivery_v);

//validate inputs - must contain all required fields
/*
	 Variable is set if it has been assigned a value other than NULL.
	 Even if a variable is assigned an empty string (""), it is set.

	 Variable is empty if it is an empty string (""), 0, "0", false, NULL, array(), and a variable declared but not given a value are all empty.

	 empty() function: returns true for the following values:
	 empty string (""), 0, "0", NULL, or FALSE

	 isset() function: returns false when the variable is NULL, that is, not initialized or given a value.*/

//trim then check length of textarea: if!strlen(trim($crs_description_v)) NO DATA!
//http://stackoverflow.com/questions/8283564/how-do-you-check-if-textarea-is-empty
if 
(
	empty($ins_id_v) ||
	empty($crs_id_v) ||
	empty($crp_pref_v) ||
	empty($crp_delivery_v)	
) 
{
  $error = "Please check <span style='color:red; font-weight: bold;'>*</span>required fields and try again.";
	//	header('Location: ../global/error.php');
  include('../global/error.php');
} 

else if ($valid_ins_id_v === false)
{
  $error = 'Error in pattern!';
  include('../global/error.php');
}

else if ($valid_ins_id_v === 0)
{
  $error = 'Must select an instructor. Please use back arrow on browser.';
  include('../global/error.php');
} 

else if ($valid_crs_id_v === false)
{
  $error = 'Error in pattern!';
  include('../global/error.php');
}

else if ($valid_crs_id_v === 0)
{
  $error = 'Must select a course. Please use back arrow on browser.';
  include('../global/error.php');
} 

else if ($valid_crp_pref_v === false)
{
  $error = 'Error in pattern!';
  include('../global/error.php');
}

else if ($valid_crp_pref_v === 0)
{
  $error = 'Course preference can only be yes, no, or undecided.';
  include('../global/error.php');
} 

else if ($valid_crp_delivery_v === false)
{
  $error = 'Error in pattern!';
  include('../global/error.php');
}

else if ($valid_crp_delivery_v === 0)
{
  $error = 'Course delivery can only be f2f, online, or both.';
  include('../global/error.php');
} 

else
{		
  //pull in db connection
	require_once "../global/connection.php";
	//pull in function library
  require_once "../global/functions.php";

	//stop processing to test input values
	//exit($ins_id_v . ", " . $crs_id_v . ", " . $crp_pref_v . ", " . $crp_delivery_v . ", " . $crp_notes_v);	

	addCoursePref
	(
		$ins_id_v,
		$crs_id_v,
		$crp_pref_v,
		$crp_delivery_v,
		$crp_notes_v
	);
} 
?>
