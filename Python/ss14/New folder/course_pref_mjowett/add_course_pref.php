<?php include_once("../global/error_display.php"); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="FSU's iSchool Instructor Course Preference System">
		<meta name="author" content="DIS Team: LIS4905_5900 Fall 2019">
		<link rel="icon" href="favicon.ico">

		<title>FSU's iSchool Instructor Course Preference System</title>
	
	<!--Include css file-->
	<?php include_once("../css/include_css.php");?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>
	
	<div class="container">
		<div class="starter-template">
			<div class="row">
				<div class="col-xs-12">

					<div class="page-header">
						<?php include_once("../global/header.php"); ?>	
					</div>

					<h3>Add Course Preference</h3>						
					<span style="color:red; font-weight: bold;">*</span> = required
					<form id="add_course_pref" method="post" class="form-horizontal" action="add_course_pref_process.php"  autocomplete="on">
						
						<?php
						//make sure file is only required once,
						//fail causes error that stops remainder of page from precessing
						require_once "../global/connection.php";
						
						//pull in function library
						require_once "../global/functions.php";
						/*
							 Best practice: sanitize input (prepare statements), and escape output (htmlspecialchars())
							 Call htmlspecialchars() when echoing data into HTML.
							 However, don't store escaped HTML in your database.
							 The database should store actual data, not its HTML representation.
							 Also, helps protect against cross-site scripting (XSS).
							 XSS enables attackers to inject client-side script into Web pages viewed by other users
						 */
						?>
						<div class="form-group">
							<label class="col-sm-4 control-label">
								<span style="color:red; font-weight: bold;">*</span>Instructor:
							</label>
							<div class="col-sm-4">
								<?php
								//call UDF (user-defined function)
								$sql = "SELECT ins_id, ins_fname, ins_lname FROM instructor ORDER BY ins_lname";								
								$results = getResultSet($sql);
								//print_r($results);  //for testing
								//exit();
								?>
								<select class='form-control' name="ins_id" >
									<option>Make selection...</option>
									<?php
									foreach($results as $result) :
									?>
										<option value="<?php echo $result['ins_id']; ?>">
											<?php echo htmlspecialchars($result['ins_lname']); ?>,
											<?php echo htmlspecialchars($result['ins_fname']); ?>
										</option>
									<?php
									endforeach;
									?>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">
								<span style="color:red; font-weight: bold;">*</span>Course:
							</label>
							<div class="col-sm-4">
								<?php
								//call UDF (user-defined function)
								$sql = "SELECT crs_id, crs_num, crs_name FROM course ORDER BY crs_num";								
								$results = getResultSet($sql);
								//print_r($results);  //for testing
								//exit();
								?>
								<select class='form-control' name="crs_id" >
									<option>Make selection...</option>
									<?php
									foreach($results as $result) :
									?>
										<option value="<?php echo $result['crs_id']; ?>">
											<?php echo htmlspecialchars($result['crs_num']); ?> - 
											<?php echo htmlspecialchars($result['crs_name']); ?>
										</option>
									<?php
									endforeach;
									?>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">
								<span style="color:red; font-weight: bold;">*</span>Course Preference:
							</label>
							<div class="col-sm-4">
								<select class="form-control" name="crp_pref">
									<option value="y" selected="selected">Yes</option>
									<option value="n">No</option>
									<option value="u">Undecided</option>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">
								<span style="color:red; font-weight: bold;">*</span>Delivery:
							</label>
							<div class="col-sm-4">
								<select class="form-control" name="crp_delivery">
									<option value="f2f" selected="selected">f2f</option>
									<option value="online">online</option>
									<option value="both">both</option>
								</select>
							</div>
						</div>
						
						
						<div class="form-group">
							<label class="col-sm-4 control-label">Notes:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" maxlength="255" name="crp_notes" />
							</div> 
						</div>

						<div class="form-group">
							<div class="col-sm-6 col-sm-offset-3">
								<button type="submit" class="btn btn-primary" name="add" value="Add">Add</button>
							</div>
						</div>
					</form>
					<!-- 
							 When using the formValidation script,
							 Do NOT use name="submit" or id="submit" attribute for the submit button
							 Otherwise, the form can't be submitted after validation!
					-->
					
				</div>
			</div>

			<?php include_once "../global/footer.php"; ?>

		</div> <!-- end starter-template -->
	</div> <!-- end container -->
	
	<!--Include the js fiile-->
	<?php //include_once("../js/include_js.php");?>

	<!-- Bootstrap JavaScript
			 ================================================== -->
	<!-- Placed at end of document so pages load faster -->

	<script type="text/javascript">

	 //client-side data validation goes here...
	 $(document).ready(function() {
		 //http://formvalidation.io/getting-started/
		 //http://regexone.com/
		 $('#add_course_pref').formValidation({
			 message: 'This value is not valid',
			 icon: {//indicate which Font Awesome icons to use
				 valid: 'fa fa-check',
							invalid: 'fa fa-times',
							validating: 'fa fa-refresh'
			 },
			 fields: {
				 ins_id: {
					 validators: {
						 notEmpty: {
							 message: 'Instructor required.'
						 },
						 stringLength: {
							 min: 1,
							 max: 5,
							 message: 'Instructor ID must be no more than 5 digits.'
						 },
						 regexp: {//see regexp examples below (here: must include 1-5 digits)
							 regexp: /^[\d{1,5}]+$/,
											message: 'Can only contain numbers.'
						 },									
					 },
				 },
				 
				 crs_id: {
					 validators: {
						 notEmpty: {
							 message: 'Course ID required.'
						 },
						 stringLength: {
							 min: 1,
							 max: 5,
							 message: 'Course ID must be no more than 5 digits.'
						 },
						 regexp: {
							 regexp: /^[\d{1,5}]+$/,
							 message: 'Can only contain numbers.'
						 },									
					 },
				 },
 				 
			 }
		 });
	 });
	</script>
</body>
</html>
