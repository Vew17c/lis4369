<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));

//Get item data
//No need for pst_id when adding, uses auto incriment
$prg_id_v = $_POST['prg_id'];
$prg_name_v = $_POST['name'];
$prg_notes_v = $_POST['notes'];

//exit($crs_name_v.",".$crs_id_v);

//Validation for Server side
$pattern='/^[a-zA-Z\-\s]+$/';
$valid_name = preg_match($pattern, $prg_name_v);

//echo $valid_name, $valid_city, $valid_email, $valid_phone, $valid_state, $valid_street, $valid_url, $valid_ytdsales, $valid_zip;
//exit();

if (
    empty($prg_name_v)
){
    $error = "Course Name required. Check all fields and try again.";
    include('../global/error.php');
} 
else if ($valid_name === false){
    echo "Error in the program name!"; include('../global/error.php');
}
else {

require_once('../global/connection.php');

$query = 
"UPDATE program
 SET
   prg_name = :prg_name_p, 
   prg_notes = :prg_notes_p
  WHERE prg_id = :prg_id_p";

//exit($query);

try
    {
    $statement = $db->prepare($query);
    $statement->bindParam(':prg_id_p', $prg_id_v);
    $statement->bindParam(':prg_name_p', $prg_name_v);
    $statement->bindParam(':prg_notes_p', $prg_notes_v);
    $statement->execute();
    $statement->closeCursor();

    $last_auto_increment_id = $db->lastInsertId();
    //include('index.php'); //forwarding is faster, one trip to server
    header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)
    }
    catch (PDOException $e)
    {
        $error = $e->getMessage();
        echo $error;
    }
}
?>
