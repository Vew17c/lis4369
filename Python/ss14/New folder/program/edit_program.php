<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Your Name Here!">
	<link rel="icon" href="../img/favicon.ico">

	<title>LIS4905 - Project</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("../global/header.php"); ?>	
					</div>

					<h2>Program</h2>

						<form id="defaultForm" method="post" class="form-horizontal" action="edit_program_process.php">

                        <?php
                        require_once "../global/connection.php";

                        //capture's ID
                        $prg_id_v = $_POST['prg_id'];

                        //find all data associated with selected petstore ID above
						$query = 
                        "SELECT * 
                        FROM program
                        WHERE prg_id = :prg_id_p";

                        $statement = $db->prepare($query);
                        $statement->bindParam(':prg_id_p', $prg_id_v);
                        $statement->execute();
                        $result = $statement->fetch();
                        while($result != null)
                        {
                            ?>
                                <input type="hidden" name="prg_id" value="<?php echo $result['prg_id']; ?>" />

								<div class="form-group">
										<label class="col-sm-4 control-label">Program Name:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="name" value="<?php echo $result['prg_name']; ?>"/>
										</div>
                                </div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Notes:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="notes" value="<?php echo $result['prg_notes']; ?>"/>
										</div>
								</div>

                                <?php
                                $result = $statement->fetch();
                                }
                                $db = null;
                                ?>


								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-3">
									<button type="submit" class="btn btn-primary" name="edit" value="Edit">Update</button>
										</div>
								</div>
						</form>

			<?php include_once "../global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("../js/include_js.php"); ?>

<script type="text/javascript">
 //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
 $(document).ready(function() {

	$('#defaultForm').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {
					fields: {
					fname: {
							validators: {
									notEmpty: {
									 message: 'Program name required'
									},
									stringLength: {
											min: 1,
											max: 45,
									 message: 'Name no more than 30 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[\w\-\s]+$/,
										message: 'Name can only contain letters, numbers, hyphens, and underscore'
									}									
							}
					},

					notes: {
							validators: {
									stringLength: {
											min: 1,
											max: 255,
											message: 'Notes'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[\w\-\s]+$/,		
									message: 'Notes'
									}									
							}
					}
					
			}
	});
});
</script>

</body>
</html>
