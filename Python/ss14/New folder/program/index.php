<?php
require_once "../global/connection.php";
$query = "SELECT * FROM program ORDER BY prg_id";

$statement = $db->prepare($query);
$statement->execute();
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Testing instructor With a Mock Index">
	<meta name="author" content="DIS team">
	<link rel="icon" href="favicon.ico">

		<title>Test Program</title>
		<?php include_once("../css/include_css_data_tables.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>
	
	<div class="container-fluid">
		 <div class="starter-template">
						<div class="page-header">
							<?php include_once("../global/header.php"); ?>	
						</div>

						<h2>Program</h2>

<a href="add_program.php">Add program</a>
<br />

 <div class="table-responsive">
	 <table id="myTable" class="table table-striped table-condensed" >

		 <!-- Code displaying PetStore data with Edit/Delete buttons goes here // -->
		 <thead>
		 <tr>
		 <th class="text-center">Name</th>
		 <th class="text-center">Notes</th>
		 <th>&nbsp</th>
		 <th>&nbsp</th>
		 </tr>
		 </thead>

		 <?php
		 $result = $statement->fetch();
		 while($result != null)
		 {
			?>
			<tr>
			<td class="text-left"><?php echo htmlspecialchars($result['prg_name']); ?></td>
			<td class="text-left"><?php echo htmlspecialchars($result['prg_notes']); ?><td>

			<td>
				<form
				onsubmit = "return confirm('Do you really want to delete record?');"
				action="delete_program.php"
				method="post"
				id="delete_program">

					<input type="hidden" name="prg_id" value="<?php echo $result['prg_id']; ?>" />
					<input type="submit" value="Delete" />
				</form>
			</td>

			<td>
				<form action="edit_program.php" method="post" id="edit_program">

					<input type="hidden" name="prg_id" value="<?php echo $result['prg_id']; ?>" />
					<input type="submit" value="Edit" />
				</form>
			</td>

			</tr>
			<?php	
				$result = $statement->fetch();
		 }
		 $statement->closeCursor();
		 $db = null;

		 ?>


	 </table>
 </div> <!-- end table-responsive -->
 	
<?php
include_once "../global/footer.php";
?>

			</div> <!-- end starter-template -->
  </div> <!-- end container -->

	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
		<?php include_once("../js/include_js_data_tables.php"); ?>	

		<script type="text/javascript">
	 $(document).ready(function(){
		 $('#myTable').DataTable({
			// "pageLength": 5
			 //https://datatables.net/reference/option/lengthMenu
			 //1st inner array page length values; 2nd inner array displayed options
			 //Note: -1 is used to disable pagination (i.e., display all rows)
			//Note: pageLength property automatically set to first value given in array
		 "lengthMenu": [ [ 10, 25, 50, -1], [ 10, 25, 50, "All"] ],
	 //permit sorting (i.e., no sorting on last two columns: delete and edit)
    "columns":
		[
		null,			
		null,
     { "orderable": false },
     { "orderable": false }			
    ]
		 });
});
	</script>

</body>
</html>
