<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="FSU's iSchool Instructor Course Preference System">
		<meta name="author" content="DIS Team: LIS4905_5900 Fall 2019">
		<link rel="icon" href="favicon.ico">

		<title>FSU's iSchool Instructor Course Preference System</title>
		<?php include_once("../css/include_css.php"); ?>
	</head>
	<body>
		<?php include_once("nav.php"); ?>
		
		<div class="container">
			<div class="row">
				<div class=".col-xs-12 col-sm-offset-2">
					<div class="page-header">
						<?php include_once("header.php"); ?>	
					</div>

					<h2 class="top">Error!</h2>

					<?php echo $error; ?>
					<br /><br />

					<?php
					require_once "footer.php";
					?>

				</div><!-- end grids -->
			</div><!-- end row -->
		</div><!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->
		<?php include_once("../js/include_js.php"); ?>

	</body>
</html>
