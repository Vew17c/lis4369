<?php
//Note: JavaDoc-like comment syntax
//https://www.oracle.com/technetwork/java/javase/documentation/index-137868.html

/**
* Function Utility Library
*
* Package contains functions used for manipulating data.
*
* @package global
* @subpackage none: included only for example
*/

//used to display errors
include_once("error_display.php");

// ##### BEGIN getResultSet #####

/**
* Get result sets
*
* @author Mark Jowett <mjowett@fsu.edu>
	* @param string $query
	* @return string
	*/
	function getResultSet($query)
	{
  //make $db available inside function
  global $db;

	//Note: (Bootstrap) responsive DataTables automatically sort by first column in displayed table)

  try 
  {
	//with large query result sets, loop through fetch() rather than fetchAll():
	//fetchAll() returns array of arrays
	//http://php.net/manual/en/pdostatement.bindcolumn.php
	
	//because no user entered data, no need to bind values
	$statement = $db->prepare($query);
	$statement->execute();
	$statement->setFetchMode(PDO::FETCH_ASSOC);
	$result = $statement->fetchAll();
	$statement->closeCursor();
	return $result;
  } 

  catch (PDOException $e) 
  {
  $error = $e->getMessage();
  display_db_error($error);
  }
	}
	// ##### END getResultSet #####

	// ##### BEGIN addCoursePref #####

	/**
	* Add course preference
	*
	* @author Mark Jowett <mjowett@fsu.edu>
	* @param int $ins_id_v
	* @param int $crs_id_v
	* @param string $crp_pref_v
	* @param string $crp_delivery_v
	* @param string $crp_notes_v
	*/

	//Note: does *not* require PK, as it is auto incremented!
function addCoursePref
(
	$ins_id_v,
	$crs_id_v,
	$crp_pref_v,
	$crp_delivery_v,
	$crp_term_v,
	$crp_day_v,
	$crp_time_of_day_v,
	$crp_start_time_v,
	$crp_end_time_v,
	$crp_room_v,
	$crp_pref_typical_v,
	$crp_notes_v
)
{
	global $db;
	$query =
		"INSERT INTO course_pref 
			(ins_id, crs_id, crp_pref, crp_delivery, crp_term, crp_day, crp_time_of_day, crp_start, crp_end, crp_room, crp_typical, crp_notes)
		 VALUES
		 	(:ins_id_p, :crs_id_p, :crp_pref_p, :crp_delivery_p, :crp_term_p, :crp_day_p, :crp_time_of_day_p, :crp_start_time_p, :crp_end_time_p, :crp_room_p, :crp_pref_typical_p, :crp_notes_p)";
	  try
		{
		$statement = $db->prepare($query);
		//$statement->bindParam(':crp_id_p',$crp_id_v);
		$statement->bindParam(':crs_id_p',$crs_id_v);
		$statement->bindParam(':ins_id_p',$ins_id_v);
		$statement->bindParam(':crp_pref_p', $crp_pref_v);
		$statement->bindParam(':crp_delivery_p', $crp_delivery_v);
		$statement->bindParam(':crp_term_p', $crp_term_v);
		$statement->bindParam(':crp_day_p', $crp_day_v);
		$statement->bindParam(':crp_time_of_day_p', $crp_time_of_day_v);
		$statement->bindParam(':crp_start_time_p', $crp_start_time_v);
		$statement->bindParam(':crp_end_time_p', $crp_end_time_v);
		$statement->bindParam(':crp_room_p', $crp_room_v);
		$statement->bindParam(':crp_pref_typical_p', $crp_pref_typical_v);
		$statement->bindParam(':crp_notes_p',$crp_notes_v);
		$statement->execute();
		$statement->closeCursor();
		
		$last_auto_increment_id = $db->lastInsertId();
		//include('index.php'); //forwarding is faster, one trip to server
		header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)
		}
		catch (PDOException $e)
		{
			$error = $e->getMessage();
			echo $error;
		}
	//print_r($db->errorInfo());
	
	//include('index.php'); //forwarding is faster, one trip to server
	header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)				
}
// ##### END addCoursePref #####



function addProgramID($crs_id, $prg_id_array){
	# copied from mjowett, for the most part
	global $db;
	$i=0;
	#print_r($prg_id_array);
	#implode(', ',$prg_id_array);
	foreach($prg_id_array as $prg_id) {
		#echo("first for loop");
		foreach($prg_id as $id) {
		$query = "INSERT INTO program_course
		 (prg_id, crs_id)
		 VALUES
		 (:prg_id_p, :crs_id_p);";
		 #echo("TESTING");
		 #echo($id['prg_id'])."ID";
		 

		 try
		 {
			$statement = $db->prepare($query);
			$statement->bindParam(':prg_id_p',$id['prg_id']);
			$statement->bindParam(':crs_id_p',$crs_id);
			$statement->execute();
			$statement->closeCursor();
			#$i++;
		 }
		 catch (PDOException $e)
		{
			$error = $e->getMessage();
			echo $error;
		}
	
		} // end of inner loop
	} // end for each
}

function editProgramID($crs_id, $prg_id_array){
	# copied from mjowett, for the most part
	global $db;
	$i=0;
	#print_r($prg_id_array);
	#implode(', ',$prg_id_array);

	$query = "DELETE FROM program_course
	WHERE crs_id = :crs_id_p";

	try {
		$statement = $db->prepare($query);
		$statement->bindParam(':crs_id_p',$crs_id);
   		$statement->execute();
   		$statement->closeCursor();
	} catch (PDOException $e) {
		$error = $e->getMessage();
   		echo $error;
	}

	//exit(print($query));

	foreach($prg_id_array as $prg_id) {
		#echo("first for loop");
		foreach($prg_id as $id) {
		$query = "INSERT INTO program_course
		 (prg_id, crs_id)
		 VALUES
		 (:prg_id_p, :crs_id_p);";
		 #echo("TESTING");
		 #echo($id['prg_id'])."ID";
		 

		 try
		 {
			$statement = $db->prepare($query);
			$statement->bindParam(':prg_id_p',$id['prg_id']);
			$statement->bindParam(':crs_id_p',$crs_id);
			$statement->execute();
			$statement->closeCursor();
			#$i++;
		 }
		 catch (PDOException $e)
		{
			$error = $e->getMessage();
			echo $error;
		}
	
		} // end of inner loop
	} // end for each
}

function addCourse
(
	$crs_num_v,
	$crs_name_v,
	$crs_credit_hrs_v,
	$crs_steward_v,
	$crs_required_v,
	$crs_degree_v,
	$crs_iep_v,
	$crs_accreditation_v,
	$crs_delivery_v,
	$crs_notes_v,
	$prg_id_array
)
{
	global $db;
	$query = 
"INSERT INTO course
(crs_num, crs_name, crs_credit_hrs, crs_steward, crs_required, crs_degree, crs_iep, crs_accreditation, crs_delivery, crs_notes)
VALUES
(:crs_num_p, :crs_name_p, :crs_credit_hrs_p, :crs_steward_p, :crs_required_p, :crs_degree_p, :crs_iep_p, :crs_accreditation_p, :crs_delivery_p, :crs_notes_p)";
	try
	{
		$statement = $db->prepare($query);
		$statement->bindParam(':crs_num_p', $crs_num_v);
		$statement->bindParam(':crs_name_p', $crs_name_v);
		$statement->bindParam(':crs_credit_hrs_p', $crs_credit_hrs_v);
		$statement->bindParam(':crs_steward_p', $crs_steward_v);
		$statement->bindParam(':crs_required_p', $crs_required_v);
		$statement->bindParam(':crs_degree_p', $crs_degree_v);
		$statement->bindParam(':crs_iep_p', $crs_iep_v);
		$statement->bindParam(':crs_accreditation_p', $crs_accreditation_v);
		$statement->bindParam(':crs_delivery_p', $crs_delivery_v);
		$statement->bindParam(':crs_notes_p', $crs_notes_v);
		$statement->execute();
		$statement->closeCursor();
	
		$last_auto_increment_id = $db->lastInsertId();

		# add program ids to prg_course
		addProgramID($last_auto_increment_id, $prg_id_array);
		//include('index.php'); //forwarding is faster, one trip to server
		header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)
		}
		catch (PDOException $e)
		{
			$error = $e->getMessage();
			echo $error;
		}
	//print_r($db->errorInfo());
	
	//include('index.php'); //forwarding is faster, one trip to server
	header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)				
}
// ##### END addCoursePref #####
?>

