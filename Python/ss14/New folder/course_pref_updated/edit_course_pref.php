<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
		<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
		<meta name="author" content="DIS Team: LIS4905_5900 Fall 2019">
		<link rel="icon" href="favicon.ico">
		<title>FSU's iSchool Instructor Course Preference System</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("../global/header.php"); ?>	
					</div>

					<h2>Edit Course Preference</h2>

						<form id="add_course_pref" method="post" class="form-horizontal" action="edit_course_pref_process.php">
						<?php
						//make sure file is only required once,
						//fail causes error that stops remainder of page from precessing
						require_once "../global/connection.php";
						
						//pull in function library
						require_once "../global/functions.php";
						/*
							 Best practice: sanitize input (prepare statements), and escape output (htmlspecialchars())
							 Call htmlspecialchars() when echoing data into HTML.
							 However, don't store escaped HTML in your database.
							 The database should store actual data, not its HTML representation.
							 Also, helps protect against cross-site scripting (XSS).
							 XSS enables attackers to inject client-side script into Web pages viewed by other users
						 */
						?>

                        <?php
                        require_once "../global/connection.php";

                        //capture's ID
                        $crp_id_v = $_POST['crp_id'];

                        //find all data associated with selected petstore ID above
						$query = 
                        "SELECT * 
                        FROM course_pref
                        WHERE crp_id = :crp_id_p";

                        $statement = $db->prepare($query);
                        $statement->bindParam(':crp_id_p', $crp_id_v);
                        $statement->execute();
                        $result = $statement->fetch();
                        while($result != null)
                        {
                            ?>
                         <input type="hidden" name="crp_id" value="<?php echo $result['crp_id']; ?>" />
						<div class="form-group">
							<label class="col-sm-4 control-label">
								<span style="color:red; font-weight: bold;">*</span>Instructor:
							</label>
							<div class="col-sm-4">
								<?php
								//call UDF (user-defined function)
								$sql = "SELECT ins_id, ins_fname, ins_lname FROM instructor ORDER BY ins_lname";								
								$results = getResultSet($sql);
								//print_r($results);  //for testing
								//exit();
								?>
								<select class='form-control' name="ins_id" >
									<!-- option>Make selection...</option -->
									<?php
									$loop_v = 1;
									foreach($results as $result1) :
									?>
										<option value="<?php echo $result1['ins_id']; ?>" <?php if ($result['ins_id'] == $loop_v) {echo 'selected';} $loop_v += 1; ?>>
											<?php echo htmlspecialchars($result1['ins_lname']); ?>,
											<?php echo htmlspecialchars($result1['ins_fname']); ?>
										</option>
									<?php
									endforeach;
									?>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">
								<span style="color:red; font-weight: bold;">*</span>Course:
							</label>
							<div class="col-sm-4">
								<?php
								//call UDF (user-defined function)
								$sql = "SELECT crs_id, crs_num, crs_name FROM course ORDER BY crs_num";								
								$results = getResultSet($sql);
								//print_r($results);  //for testing
								//exit();
								?>
								<select class='form-control' name="crs_id" >
									<option>Make selection...</option>
									<?php
									$loop_v = 1; 
									foreach($results as $result2) :
									?>
										<option value="<?php echo $result2['crs_id']; ?>" <?php if ($result['crs_id'] == $loop_v) {echo 'selected';} $loop_v += 1; ?>>
											<?php echo htmlspecialchars($result2['crs_num']); ?> - 
											<?php echo htmlspecialchars($result2['crs_name']); ?>
										</option>
									<?php
									endforeach;
									?>
								</select>
							</div>
						</div>

								<div class="form-group">
									<label class="col-sm-4 control-label">
										<span style="color:red; font-weight: bold;">*</span>Course Preference:
									</label>
									<div class="col-sm-4">
										<select class="form-control" name="crp_pref">
											<option value="y" <?php if ($result['crp_pref'] == "y") { echo 'selected'; } ?>>Yes</option>
											<option value="n" <?php if ($result['crp_pref'] == "n") { echo 'selected'; } ?>>No</option>
											<option value="u" <?php if ($result['crp_pref'] == "u") { echo 'selected'; } ?>>Undecided</option>
										</select>
									</div>
								</div>
                                
                               <div class="form-group">
									<label class="col-sm-4 control-label">
										<span style="color:red; font-weight: bold;">*</span>Delivery:
									</label>
									<div class="col-sm-4">
										<select class="form-control" name="crp_delivery">
											<option value="f2f" <?php if ($result['crp_delivery'] == "f2f") { echo 'selected'; } ?>>F2F</option>
											<option value="online" <?php if ($result['crp_delivery'] == "online") { echo 'selected'; } ?>>Online</option>
											<option value="both" <?php if ($result['crp_delivery'] == "both") { echo 'selected'; } ?>>Both</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-4 control-label">
										<span style="color:red; font-weight: bold;">*</span>Term:
									</label>
									<div class="col-sm-4">
										<select class="form-control" name="crp_term">
											<option value="fall" <?php if ($result['crp_term'] == "fall") { echo 'selected="selected"'; } ?>>Fall</option>
											<option value="spr"  <?php if ($result['crp_term'] == "spr") { echo 'selected="selected"'; } ?>>Spring</option>
											<option value="smr"  <?php if ($result['crp_term'] == "smr") { echo 'selected="selected"'; } ?>>Summer</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-4 control-label">
										<span style="color:red; font-weight: bold;">*</span>Day:
									</label>
									<div class="col-sm-4">
										<select class="form-control" name="crp_day">
											<option value="m/w" <?php if ($result['crp_day'] == "m/w") { echo 'selected="selected"'; } ?>>monday/wednesday</option>
											<option value="m/w/f" <?php if ($result['crp_day'] == "m/w/f") { echo 'selected="selected"'; } ?>>monday/wednesday/friday</option>
											<option value="t/r" <?php if ($result['crp_day'] == "t/r") { echo 'selected="selected"'; } ?>>tuesday/thursday</option>
											<option value="any" <?php if ($result['crp_day'] == "any") { echo 'selected="selected"'; } ?>>anyday</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">
										<span style="color:red; font-weight: bold;">*</span>Time of day:
									</label>
									<div class="col-sm-4">
										<select class="form-control" name="crp_time_of_day" >
											<option value="morn" <?php if ($result['crp_time_of_day'] == "morn") { echo 'selected'; } ?> >Morning</option>
											<option value="aft"  <?php if ($result['crp_time_of_day'] == "aft") { echo 'selected'; } ?> >Afternoon</option>
											<option value="eve"  <?php if ($result['crp_time_of_day'] == "eve") { echo 'selected'; } ?> >Evening</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">
										<span style="color:red; font-weight: bold;">*</span>Start time:
									</label>
									<div class="col-sm-4">
											<input type="text" class="form-control" name="crp_start_time" id="timepicker" value="<?php echo $result['crp_start']; ?>" />
												<script>
													$('#timepicker').timepicker();	
												</script>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">
										<span style="color:red; font-weight: bold;">*</span>End time:
									</label>
									<div class="col-sm-4">
										<div>
											<input type="text" class="form-control" name="crp_end_time" id="timepicker1" value="<?php echo $result['crp_end']; ?>" />
												<script>
													$('#timepicker1').timepicker();	
												</script>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">
										<span style="color:red; font-weight: bold;"></span>Room:
									</label>
									<div class="col-sm-4">
										<input type="text" class="form-control" name="crp_room" value="<?php echo $result['crp_room']; ?>"/>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">
										<span style="color:red; font-weight: bold;">*</span>Typical:
									</label>
									<div class="col-sm-4">
										<select class="form-control" name="crp_pref_typical">
											<option value="y" <?php if ($result['crp_typical'] == "both") { echo 'selected'; } ?>>Yes</option>
											<option value="n" <?php if ($result['crp_typical'] == "both") { echo 'selected'; } ?>>No</option>
										</select>
									</div>
								</div>
								<div class="form-group">
										<label class="col-sm-4 control-label">Notes:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="crp_notes" value="<?php echo $result['crp_notes']; ?>"/>
										</div>
								</div>
                                <?php
                                $result = $statement->fetch();
                                }
                                $db = null;
                                ?>


								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-3">
									<button type="submit" class="btn btn-primary" name="edit" value="Edit">Update</button>
										</div>
								</div>
						</form>

			<?php include_once "../global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("../js/include_js.php"); ?>

<script type="text/javascript">
	//client-side data validation goes here...
	 $(document).ready(function() {
		 //http://formvalidation.io/getting-started/
		 //http://regexone.com/
		 $('#add_course_pref').formValidation({
			 message: 'This value is not valid',
			 icon: {//indicate which Font Awesome icons to use
							valid: 'fa fa-check',
							invalid: 'fa fa-times',
							validating: 'fa fa-refresh'
			 },
			 fields: {
				 ins_id: {
					 validators: {
						 notEmpty: {
							 message: 'Instructor required.'
						 },
						 stringLength: {
							 min: 1,
							 max: 5,
							 message: 'Instructor ID must be no more than 5 digits.'
						 },
						 regexp: {//see regexp examples below (here: must include 1-5 digits)
							 regexp: /^[\d{1,5}]+$/,
											message: 'Can only contain numbers.'
						 },									
					 },
				 },
				 
				 crs_id: {
					 validators: {
						 notEmpty: {
							 message: 'Course ID required.'
						 },
						 stringLength: {
							 min: 1,
							 max: 5,
							 message: 'Course ID must be no more than 5 digits.'
						 },
						 regexp: {
							 regexp: /^[\d{1,5}]+$/,
							 message: 'Can only contain numbers.'
						 },									
					 },
				 },
				 
				 crp_start_time: {
					 validators: {
						 notEmpty: {
							 message: 'Course start time required.'
						 },
						 stringLength: {
							 min: 1,
							 max: 8,
							 message: 'Course start time must be no more than 5 digits (with one being ":").'
						 },
						 regexp: {
							 regexp: /^((([0[0-9]|1[0-9]|2[0-3]):[0-5][0-9])|(24:00))$/,
							 message: 'Please enter valid format'
						 },									
					 },
				 },
				 
				 crp_end_time: {
					 validators: {
						 notEmpty: {
							 message: 'Course start time required.'
						 },
						 stringLength: {
							 min: 1,
							 max: 8,
							 message: 'Course end time must be no more than 5 digits (with one being ":").'
						 },
						 regexp: {
							 regexp: regexp: /^((([0[0-9]|1[0-9]|2[0-3]):[0-5][0-9])|(24:00))$/,
							 message: 'Please enter valid format'
						 },									
					 },
				 },
				 
				crp_room: {
					validators: {
						notEmpty: {
							message: 'Course Preference Room is required'
							},
						stringLength: {
							min: 1,
							max: 10,
							message: 'Course Preference Room no more than 10 characters'
							},
						regexp: {
						      regexp: /^[ a-zA-Z0-9]+$/,
							  message: 'Course Preference room can only contain letters and numbers'
						},									
					},
				}
 				 
			 }
		 });
	 });
	</script>
</body>
</html>
