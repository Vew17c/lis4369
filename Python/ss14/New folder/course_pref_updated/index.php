<?php
require_once "../global/connection.php";
$query = "select crp_id,crp_pref,crp_delivery,crp_term,crp_day,crp_time_of_day,crp_start,crp_end,crp_room,crp_typical,crp_notes,ins_lname from course_pref inner join instructor on course_pref.ins_id = instructor.ins_id ORDER BY crp_id";
$query1 = "select crs_num from course_pref inner join course on course_pref.crs_id = course.crs_id order by crp_id";
$query2 = "select ins_fname from course_pref inner join instructor on course_pref.ins_id = instructor.ins_id ORDER BY crp_id";
$statement = $db->prepare($query);
$statement1 = $db->prepare($query1);
$statement2 = $db->prepare($query2);
$statement->execute();
$statement1->execute();
$statement2->execute();
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Testing instructor With a Mock Index">
	<meta name="author" content="DIS team">
	<link rel="icon" href="favicon.ico">

		<title>Test Instructor</title>
		<?php include_once("../css/include_css_data_tables.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>
	
	<div class="container-fluid">
		 <div class="starter-template">
						<div class="page-header">
							<?php include_once("../global/header.php"); ?>	
						</div>

						<h2>Course Preference</h2>

<a href="add_course_pref.php">Add Course Pref</a>
<br />

 <div class="table-responsive">
	 <table id="myTable" class="table table-striped table-condensed" >

		 <!-- Code displaying PetStore data with Edit/Delete buttons goes here // -->
		 <thead>
		 <tr>
		 <th>CrsNum</th>
		 <th>Instructor</th>
		 <th>Preference</th>
		 <th>Delivery</th>
		 <th>Term</th>
		 <th>Day</th>
		 <th>Time</th>
		 <th>Start</th>
		 <th>End</th>
		 <th>Room</th>
		 <th>Typical</th>
		 <th>Notes</th>
		 <th>&nbsp </th>
		 <th>&nbsp </th>
		 </tr>
		 </thead>

		 <?php
		 $result = $statement->fetch();
		 $result1 = $statement1->fetch();
		 $result2 = $statement2->fetch();
		 while($result != null && $result1 != null && $result2 != null)
		 {
			?>
			<tr>
			<td align="left"><?php echo htmlspecialchars($result1['crs_num']); ?></td>
			<td align="left"><?php echo htmlspecialchars($result['ins_lname']); echo "&#44; &nbsp;"; echo htmlspecialchars($result2['ins_fname']);?></td>
			<td><?php echo htmlspecialchars($result['crp_pref']); ?></td>
			<td align="left"><?php echo htmlspecialchars($result['crp_delivery']); ?></td>
			<td align="left"><?php echo htmlspecialchars($result['crp_term']); ?></td>
			<td align="left"><?php echo htmlspecialchars($result['crp_day']); ?></td>
			<td align="left"><?php echo htmlspecialchars($result['crp_time_of_day']); ?></td>
			<td><?php echo htmlspecialchars($result['crp_start']); ?></td>
			<td><?php echo htmlspecialchars($result['crp_end']); ?></td>
			<td align="left"><?php echo htmlspecialchars($result['crp_room']); ?></td>
			<td><?php echo htmlspecialchars($result['crp_typical']); ?></td>
			<td align="left"><?php echo htmlspecialchars($result['crp_notes']); ?><td>
			<td>
				<form
				onsubmit = "return confirm('Do you really want to delete record?');"
				action="delete_course_pref.php"
				method="post"
				id="delete_course_pref">

					<input type="hidden" name="crp_id" value="<?php echo $result['crp_id']; ?>" />
					<input type="submit" value="Delete" />
				</form>
			</td>

			<td>
				<form action="edit_course_pref.php" method="post" id="edit_course_pref">

					<input type="hidden" name="crp_id" value="<?php echo $result['crp_id']; ?>" />
					<input type="submit" value="Edit" />
				</form>
			</td>

			</tr>
			<?php	
				$result = $statement->fetch();
				$result1 = $statement1->fetch();
				$result2 = $statement2->fetch();
		 }
		 $statement->closeCursor();
		 $db = null;

		 ?>


	 </table>
 </div> <!-- end table-responsive -->
 	
<?php
include_once "../global/footer.php";
?>

			</div> <!-- end starter-template -->
  </div> <!-- end container -->

	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
		<?php include_once("../js/include_js_data_tables.php"); ?>	

		<script type="text/javascript">
	 $(document).ready(function(){
		 $('#myTable').DataTable({
			// "pageLength": 5
			 //https://datatables.net/reference/option/lengthMenu
			 //1st inner array page length values; 2nd inner array displayed options
			 //Note: -1 is used to disable pagination (i.e., display all rows)
			//Note: pageLength property automatically set to first value given in array
		 "lengthMenu": [ [ 10, 25, 50, -1], [ 10, 25, 50, "All"] ],
	 //permit sorting (i.e., no sorting on last two columns: delete and edit)
    "columns":
		[
		null,			
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
     { "orderable": false },
	 { "orderable": false },
     { "orderable": false }			
    ]
		 });
});
	</script>

</body>
</html>
