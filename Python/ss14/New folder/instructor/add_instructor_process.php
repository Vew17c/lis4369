<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));

//Get item data
//No need for pst_id when adding, uses auto incriment
$ins_fname_v = $_POST['fname'];
$ins_lname_v = $_POST['lname'];
$ins_phone_v = $_POST['phone'];
$ins_email_v = $_POST['email'];
$ins_notes_v = $_POST['notes'];

//exit($pst_name_v.",".$pst_city_v);

//Validation for Server side
//Name
$pattern='/^[a-zA-Z\-\s]+$/';
$valid_fname = preg_match($pattern, $ins_fname_v);
//lname
$pattern='/^[a-zA-Z\-\s]+$/';
$valid_lname = preg_match($pattern, $ins_lname_v);


//Phone
$pattern='/^\d{10}+$/';
$valid_phone = preg_match($pattern, $ins_phone_v);
//Email
$pattern='/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/';
$valid_email = preg_match($pattern, $ins_email_v);

//echo $valid_name, $valid_city, $valid_email, $valid_phone, $valid_state, $valid_street, $valid_url, $valid_ytdsales, $valid_zip;
//exit();

if (
    empty($ins_fname_v) ||
    empty($ins_lname_v) ||
    empty($ins_phone_v) ||
    empty($ins_email_v)
){
    $error = "All feilds require data, except <b>Notes</b>. Check all feilds and try again.";
    include('../global/error.php');
} 
else if ($valid_fname === false){
    echo "Error in the pattern!";
}
else if ($valid_lname === false){
    echo "Error in the pattern!";
}
else if ($valid_fname === 0){
    $error = "First name may only contain letters";
    include('../global/error.php');
}
else if ($valid_lname === 0){
    $error = "Last name may only contain letters";
    include('../global/error.php');
}
else if ($valid_phone === false){
    echo "Error in the phone number pattern!";
}
else if ($valid_phone === 0){
    $error = "You can only have 10 digits in the phone number";
    include('../global/error.php');
}
else if ($valid_email === false){
    echo "Error in the email pattern!";
}
else if ($valid_email === 0){
    $error = "Email must match the format of: xxxx@xxxx.xxx";
    include('../global/error.php');
} else {

require_once('../global/connection.php');

$query = 
"INSERT INTO instructor
(ins_fname, ins_lname, ins_phone, ins_email, ins_notes)
VALUES
(:ins_fname_p, :ins_lname_p, :ins_phone_p, :ins_email_p, :ins_notes_p)";

try
    {
    $statement = $db->prepare($query);
    $statement->bindParam(':ins_fname_p', $ins_fname_v);
    $statement->bindParam(':ins_lname_p', $ins_lname_v);
    $statement->bindParam(':ins_phone_p', $ins_phone_v);
    $statement->bindParam(':ins_email_p', $ins_email_v);
    $statement->bindParam(':ins_notes_p', $ins_notes_v);
    $statement->execute();
    $statement->closeCursor();

    $last_auto_increment_id = $db->lastInsertId();
    //include('index.php'); //forwarding is faster, one trip to server
    header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)
    }
    catch (PDOException $e)
    {
        $error = $e->getMessage();
        echo $error;
    }
}
?>
