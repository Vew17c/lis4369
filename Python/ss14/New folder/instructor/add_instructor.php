<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Will Bollock was here :)">
	<link rel="icon" href="../img/favicon.ico">

	<title>LIS4905</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("../global/header.php"); ?>	
					</div>

					<h2>Instructor</h2>

						<form id="defaultForm" method="post" class="form-horizontal" action="add_instructor_process.php">
								<div class="form-group">
										<label class="col-sm-4 control-label">First name:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="fname" placeholder="Ex: Alexander"/>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Last name:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="lname" placeholder="Ex: Dumas"/>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Phone:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="phone" placeholder="Ex: 8501234567"/>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Email:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="email" placeholder="Ex: MyEmail@Host.com"/>
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Notes:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="notes" placeholder="Text" />
										</div>
								</div>

								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-3">
									<button type="submit" class="btn btn-primary" name="add" value="Add">Add</button>
										</div>
								</div>
						</form>

			<?php include_once "../global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("../js/include_js.php"); ?>

<script type="text/javascript">
 //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
 $(document).ready(function() {

	$('#defaultForm').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {
					fname: {
							validators: {
									notEmpty: {
									 message: 'First name required'
									},
									stringLength: {
											min: 1,
											max: 30,
									 message: 'Name no more than 30 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[\w\-\s]+$/,
										message: 'Name can only contain letters, numbers, hyphens, and underscore'
									},									
							},
					},

					lname: {
							validators: {
									notEmpty: {
									 message: 'Last name required'
									},
									stringLength: {
											min: 1,
											max: 30,
									 message: 'Name no more than 30 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[\w\-\s]+$/,
										message: 'Name can only contain letters, numbers, hyphens, and underscore'
									},									
							},
					},

					phone: {
							validators: {
									notEmpty: {
											message: 'Phone number is required'
									},
									stringLength: {
											min: 10,
											max: 10,
											message: 'Phone number must be 10 numbers'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[0-9]+$/,		
									message: 'Phone number must have 10 numbers and only numbers'
									},									
							},
					},

					email: {
							validators: {
									notEmpty: {
									 message: 'Email required'
									},
									stringLength: {
											min: 1,
											max: 100,
									 message: 'Email is no more than 100 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/,
										message: 'Emails can only contain letters, numbers, @, hyphens, and underscore'
									},									
							},
					},

					notes: {
							validators: {
									stringLength: {
											min: 1,
											max: 255,
											message: 'Notes'
									},	
									message: 'Notes'
									},			
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: '/*/',
										message: 'Notes'						
							}
					}
					
			}
	});
});
</script>

</body>
</html>
