 #!/usr/bin/env python3

def get_requirements():
    print("\nProgram Requirements:")
    print("1. Use Python selection structure.")
    print("2. Prompt for two numbers, and a suitable operator.")
    print("3. Test for correct numeric operator.")
    #print("4. Format, right-align numbers, and round to two decimal places")

def calc_percentage():
    numb1 = 0
    numb2 = 0

    lck = False #Keeps the loop in cehck

    #Collects the input
    print("\nInput: ")

    while lck == False:
        try:
            numb1 = float(input("Enter num1:  "))
            lck = True
        except ValueError:
            lck = False
            print("You have entered an inccorect value. Please enter a decimal number")
    
    lck = False
    while lck == False:
        try:
            numb2 = float(input("Enter num2:  "))
            lck = True
        except ValueError:
            lck = False
            print("You have entered an inccorect value. Please enter a decimal number")
    
    lck = False

    calc_operator(numb1, numb2)


def calc_operator (numb1, numb2):
    import operator

    lck = False
    test_val = 0
    total = 0
    while lck == False:
        
        print("\nSuitable Operators: +, -, *, / , // (integer division), % (modulo operator), ** (power)")
        test_val = input("Enter operator:  ")

        test_val = test_val.lower()

        try:
            ops = { #Modify this part if you are putting in a calculation that doenst have the ability to crash the program
                "+": numb1 + numb2, 
                "-": numb1 - numb2, 
                "*": numb1 * numb2,  
                "**": numb1 ** numb2, 
                "pow": pow(numb1, numb2)
                }  

            if test_val == "/":
                if numb2 == 0: 
                    print("You cant divide by zero")
                    lck = False
                else:
                    total = numb1 / numb2
                    lck = True
            elif test_val == "//":
                if numb2 == 0: 
                    print("You cant divide by zero")
                    lck = False
                else:
                    total = numb1 // numb2
                    lck = True
            elif test_val == "%":
                if numb2 == 0: 
                    print("You cant use Modulo with a denomniator of zero")
                    lck = False
                else:
                    total = numb1 % numb2
                    lck = True
            else:
                total = ops[test_val]
                lck = True

            print("{:,.2f}".format(total))
            
        except KeyError:
            print("Invalid character inputed!")  
            lck = False