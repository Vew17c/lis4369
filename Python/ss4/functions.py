 #!/usr/bin/env python3
def get_requirements():
    print("\nProgram Requirements:")
    print("1. Find calories per grams of fat, carbs, and protien.")
    print("2. Calculate percentages.")
    print("3. Must use float data types.")
    print("4. Format, right-align numbers, and round to two decimal places")

def calc_percentage():
    ft_gram = 0
    cb_gram = 0
    pr_gram = 0
    total = 0

    ft_gram_per = 0
    cb_gram_per = 0 
    pr_gram_per = 0

    FT_CAL_TO_GRAM = 9
    CB_CAL_TO_GRAM = 4
    PR_CAL_TO_GRAM = 4

    lck = False #Keeps the loop in cehck

    #Collects the input
    print("\nInput: ")

    while lck == False:
        try:
            ft_gram = float(input("Enter number of fat grams:  "))
            lck = True
        except ValueError:
            lck = False
            print("You have entered an inccorect value. Please enter a decimal number")
    
    lck = False
    while lck == False:
        try:
            cb_gram = float(input("Enter number of carb grams:  "))
            lck = True
        except ValueError:
            lck = False
            print("You have entered an inccorect value. Please enter a decimal number")
    
    lck = False
    while lck == False:
        try:
            pr_gram = float(input("Enter number of protien grams:  "))
            lck = True
        except ValueError:
            lck = False
            print("You have entered an inccorect value. Please enter a decimal number")
    
    lck = False

    #Convert the numbers into grams
    ft_gram = ft_gram * FT_CAL_TO_GRAM
    cb_gram = cb_gram * CB_CAL_TO_GRAM
    pr_gram = pr_gram * PR_CAL_TO_GRAM

    #Find the total
    total = ft_gram + cb_gram + pr_gram

    #Calculates it_stu percentage
    ft_gram_per = (ft_gram / total)
    cb_gram_per = (cb_gram / total)
    pr_gram_per = (pr_gram / total)

    #Printing out the results
    print_cal(ft_gram, ft_gram_per, cb_gram, cb_gram_per, pr_gram, pr_gram_per)

def print_cal(ft_gram, ft_gram_per, cb_gram, cb_gram_per, pr_gram, pr_gram_per):
    print("\nOutput:")
    print ("{:<8} {:>14} {:>14}".format("Type" , "Calories", "Percentage"))
    print ("{:<8} {:>14.2f} {:>14.2%}".format("Fat" , ft_gram , ft_gram_per))
    print ("{:<8} {:>14.2f} {:>14.2%}".format("Carb" , cb_gram , cb_gram_per))
    print ("{:<8} {:>14.2f} {:>14.2%}".format("Protien" , pr_gram , pr_gram_per))
