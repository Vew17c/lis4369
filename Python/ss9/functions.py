 #!/usr/bin/env python3

def get_requirements():
    print("\nProgram Requirements:")
    print("1. Sets (Python data structure): mutable, ordered sequence of elements.")
    print("2. Sets are mutable/changeble--that is, can insert, update, delete.")
    print("3. Create sets - using parentheses {set}: my_tuple = {1, 3.14, 2.0, 'four', 'Five'}.")
    print("4. Create a program that mirrors the following IPO (input/processing/output) format.")
    print("5. Two methods to creat sets:")
    print("\ta. create sets using brackets {set}: my_set1 = set([1, 3.14, 2.0, 'four', 'Five'])")
    print("\tb. create sets using brackets {set}: my_set2 = set((1, 3.14, 2.0, 'four', 'Five'))")
    print("\tExamples: \n\tmy_set = set({1, 3.14, 2.0, 'four', 'Five'}) \n\tmy_set2 = set(({1, 3.14, 2.0, 'four', 'Five'}))")

def calculation():
    my_set  = {1, 3.14, 2.0, 'four', 'Five'}
    my_set1 = set([1, 3.14, 2.0, 'four', 'Five']) 
    my_set2 = set((1, 3.14, 2.0, 'four', 'Five'))

    print("\nOutput")
    print("Print my_set created using curly brackets: ")
    print(my_set)

    print("\nPrint type of my_set: ")
    print(type(my_set))

    print("\nPrint my_set created using set function with list: ")
    print(my_set1)

    print("\nPrint type of my_set1: ")
    print(type(my_set1))

    print("\nPrint my_set2 created using set function with tupple: ")
    print(my_set2)

    print("\nPrint type of my_set2: ")
    print(type(my_set2))

    print("\nLength of my_set: ")
    print(len(my_set))

    print("\nDiscard 'four': ")
    my_set.discard('four')
    my_set.discard('four')
    print(my_set)

    print("\nRemove 'Five': ")
    my_set.remove('Five')
    #my_set.remove('Five')
    print(my_set)

    print("\nLength of my_set: ")
    print(len(my_set))

    print("\nAdd 4 to my_set using the add() function: ")
    my_set.add(4)
    print(my_set)

    print("\nLength of my_set: ")
    print(len(my_set))

    print("\nDisplay minimum number: ")
    print(min(my_set))

    print("\nDisplay maximum number: ")
    print(max(my_set))

    print("\nDisplay sum of number: ")
    print(sum(my_set))

    print("\nDelete all set elements: ")
    my_set.clear()
    print(my_set)

    print("\nLength of my_set: ")
    print(len(my_set))