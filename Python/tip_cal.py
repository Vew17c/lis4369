#!/usr/bin/env python3

#Varibles to calculate the tip
input_bill = 0
input_tax = 0
input_tip = 0
input_party = 0
total = 0.0

#Loop varible
lock_l1 = False
lock_l2 = False

#Introduction
print('Tip Calculator \n\nProgram Requirements: \n1. Must use float data type for user input (expcept for "Party Number"')
print('2. Must round calculatios to the two decimal places. \n3. Must format currency with dollar sign, and two decimal places.\n')

#Input codes
print('User Input:')
while (lock_l1 == False):
    #Test to see if the input is an float varible
    try:
        input_bill = float(input("Cost of meal:  "))
        lock_l1 = True
    except ValueError:
        print("Invalid input. Please try again")
        lock_l1 = False

lock_l1 = False
while (lock_l1 == False):
    # Test to see if the input is an float varible
    try:
        input_tax = float(input("Tax percent:   "))
        lock_l1 = True
    except ValueError:
        print("Invalid input. Please try again")
        lock_l1 = False

lock_l1 = False
while (lock_l1 == False):
    # Test to see if the input is an float varible
    try:
        input_tip = float(input("Tip percent:   "))
        lock_l1 = True
    except ValueError:
        print("Invalid input. Please try again")
        lock_l1 = False

lock_l1 = False
while (lock_l1 == False):
    # Test to see if the input is an float varible
    try:
        input_party = int(input("Party number:  "))
        lock_l1 = True
    except ValueError:
        print("Invalid input. Please try again")
        lock_l1 = False

print("\n\nProgram Output:")
#Prints subtotal
print("Subtotal:     " , '${:,.2f}'.format(input_bill))
#Find tax
input_tax = input_bill/input_tax  
print("Tax:          " , '${:,.2f}'.format(input_tax))
#Finds and prints the ammount due
input_bill = input_bill + input_tax
print("Amount Due:   " , '${:,.2f}'.format(input_bill))
#Finds and prints the gratuity 
input_tip = (input_tip * 0.01)*input_bill
print("Gratuity:     " , '${:,.2f}'.format(input_tip))
#Finds and prints the total due
input_bill = input_bill + input_tip
print("Total:        " , '${:,.2f}'.format(input_bill))
#Finds and prints the total due
total = input_bill / input_party 
print("Split(" + str(input_party) + "):     " , '${:,.2f}'.format(total))




# Just some extra code

#while (lock_l1 == False):
    # Test to see if the input is an int varible
#    try:
#        tip = int(input("Please enter the tip ammount (Either 10, 15, or 20): "))
#        lock_l1 = True
#
        # Makes sure that the only values inputed are the listed values that were given
#        if not (tip == 10 or tip == 15 or tip == 20):
#           print("Invalid tip option. Please try again")
#            lock_l1 = False
#    except ValueError:
#        print("Invalid integer number. Please try again")
#        lock_l1 = False
