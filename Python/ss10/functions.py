#!/usr/bin/env python3
def get_requirements():
    print("\nProgram Requirements:")
    print("1. Dictionaries (Python data structure): Unordered key-value pairs.")
    print("2. Dictionary: an assisatuve array (also known as hashes).")
    print("3. Any key in a dictionary is associated (or mapped) to a value (i.e. any Python data type)")
    print("4. Keys: must be aof imutable type (string, number or tupple with imutable elements) and must be unique.")
    print("5. Values: can be any data type and can repeat.")
    print("6. Create a program that mirrors the following IPO format.")
    print("\tCreate empty dictionary, using curly brackets {}: my_dictionary = {}")
    print("\tUse the following keys: fname, lname, degree, major, gpa")

def calculation():

    fname = input("First Name: ")
    lname = input("Last Name: ")
    degree = input("Degree: ")
    major = input("Major (IT or ICT): ")
    gpa = float(input("GPA: "))

    my_dictionary = {'fname': fname , 'lname': lname , 'degree': degree , 'major': major , 'gpa': gpa}

    print("\nOutput")
    print("\nPrint my_dictonary: ")
    print(my_dictionary)

    print("\nReturn view of dictionary's (key value) pairs built-in function: ")
    print(my_dictionary.items())

    print("\nReturn view object of all keys, built-in function: ")
    print(my_dictionary.keys())

    print("\nReturn view object of all values in dictionary, built-in function: ")
    print(my_dictionary.values())

    print("\nPrint only first and last name, using keys: ")
    print(my_dictionary['fname'] + " , " + my_dictionary['lname'])

    print("\nPrint only first and last name, using get function: ")
    print(my_dictionary.get('fname', "Not found") + ", " + my_dictionary.get('lname', "Not found"))

    print("\nCount number of items (key:values pairs) in dictionary: ")
    print(len(my_dictionary.items()))

    print("\nRemove last dictionary item (popitem): ")
    my_dictionary.popitem()
    print(my_dictionary)

    print("\nRemove major from dictionary, using key: ")
    del my_dictionary['major']
    print(my_dictionary)

    print("\nReturn object type: ")
    print(type(my_dictionary))

    print("\nDelete all items from list: ")
    my_dictionary.clear()
    print(my_dictionary)
    