 #!/usr/bin/env python3

def get_requirements():
    print("\nProgram Requirements:")
    print("1. Create write, read file subdirectory with two files: main.py and function.py.\n"
    + "2. Use President Abraham Lincoln Gettsyburg Address: Full Text.\n"
    + "3. Write address to file.\n"
    + "4. Read address from same file.\n"
    + "5. Create Python Docstring for each function\n"
    + "6. Display Python Docstring for each functions.py file.\n"
    + "7. Display full path file\n"
    + "8. Replicate display below.")

def write_read_file():
    '''Usage: Calls two functions:
        1. file_write() # writes to file
        2. file_read() # reads from file
       Parameters: none
       Returns: none'''

    import os

    help(write_read_file)
    help(file_write)
    help(file_read)

    file_write()
    file_read()

    print("\n" + os.path.abspath("./skill_set_15_test.txt"))

def file_write():
    '''Usage: Creates file, and write contents of global varible to file:
       Parameters: none
       Returns: none'''
    f = open("skill_set_15_test.txt", "w")
    f.write('''President Lincoln Gettysburg Address
    
    Fourscore and seven years ago our fathers brought forth, on this continent, a new nation, conceived in liberty, and dedicated to the
    proposition that all men are created equal. 
           
    Now we are engaged in a great civil war, testing whether that nation, or any nation so conceived, and so dedicated, can long endure. 
    We are met on a great battle-field of that war. We have come to dedicate a portion of that field, as a final resting-place for 
    those who here gave their lives, that that nation might live. It is altogether fitting and proper that we should do this. 
           
    But, in a larger sense, we cannot dedicate, we cannot consecrate—we cannot hallow—this ground. The brave men, living and dead, who 
    struggled here, have consecrated it far above our poor power to add or detract. The world will little note, nor long remember what 
    we say here, but it can never forget what they did here. It is for us the living, rather, to be dedicated here to the unfinished work 
    which they who fought here have thus far so nobly advanced. It is rather for us to be here dedicated to the great task remaining 
    before us—that from these honored dead we take increased devotion to that cause for which they here gave the last full measure of 
    devotion—that we here highly resolve that these dead shall not have died in vain—that this nation, under God, shall have a new birth 
    of freedom, and that government of the people, by the people, for the people, shall not perish from the earth.
           
    Abraham Lincoln
    November 19, 1863''')
    f.close()

def file_read():
    '''Usage: Reads contents of written file:
       Parameters: none
       Returns: none'''
    f = open("skill_set_15_test.txt", "r")
    print(f.read())
