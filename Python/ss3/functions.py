 #!/usr/bin/env python3
def get_requirements():
    print("\nProgram Requirements:")
    print("1. Find number of IT/ICT students in class.")
    print("2. Calculate IT/ICT students in class")
    print("3. Must use float data type (to facilitate right allignment)")
    print("4. Format, right-align numbers, and round to two decimal places")

def calc_percentage():
    it_stu = 0
    itc_stu = 0
    total = 0

    lck = False #Keeps the loop in cehck

    #Collects the input
    print("\nInput: ")

    while lck == False:
        try:
            it_stu = float(input("Enter number of IT students:  "))
            lck = True
        except ValueError:
            lck = False
            print("You have entered an inccorect value. Please enter a decimal number")
    
    lck = False
    while lck == False:
        try:
            itc_stu = float(input("Enter number of ITC students: "))
            lck = True
        except ValueError:
            lck = False
            print("You have entered an inccorect value. Please enter a decimal number")
    
    #Find the total
    total = it_stu + itc_stu

    #Calculates it_stu percentage
    it_stu = (it_stu / total)

    #Total pay according to the hours
    itc_stu = (itc_stu / total)

    #Printing out the results
    print_cal(total, it_stu, itc_stu)

def print_cal(total, it_stu, itc_stu):
    print("\nOutput:")
    print ("{0:17} {1:>5.2f}".format("Total Students:" , total))
    print ("{0:17} {1:>5.2%}".format("IT Students:" , it_stu))
    print ("{0:17} {1:>5.2%}".format("ITC Students:" , itc_stu))
