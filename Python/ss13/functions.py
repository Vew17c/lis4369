#!/usr/bin/env python3
def get_requirements():
    print("\nProgram Requirements:")
    print("1. Program calculates sphere volume in liquid U.S. gallons from user-entered diameter value in inches,\nand rounds to two decimal places.")
    print("2. Must use Python's *built-in* PI and pow() capabilities.")
    print("3. Program checks for non-integer and non-numeric values.")
    print("4. Program continuously prompts until no longer requested.\n\n")

def calculation():
    import math

    lck = True
    lck_1 = True

    VOL_PER_GAL = 231

    print("Input: ")

    confirm = input("Do you want to calulate a sphere volume (y/n)?: ").lower()
    print("\nOutput:")

    while (lck == True):
        while (confirm == "y"):
            try:
                daim_len = int(input("Please enter diameter in inches: "))
                daim_len = daim_len/2
                daim_len = (4 * (math.pi * pow(daim_len, 3)))/3
                daim_len = daim_len / VOL_PER_GAL
                print("\nSphere volume:" , str(round(daim_len, 2)) , "liquid U.S. gallons")
            except ValueError:
                print("\nIncorrect Value. Please try again.")
                break


            while (lck_1 == True):
                confirm = input("\nDo you want to calculate another sphere volume (y/n)?: ").lower()
                if confirm == "y":
                    lck = True
                    break
                elif confirm == "n":
                    lck = False
                    break
                else:
                    print("\nIncorrect Value. Please try again.")
                    continue
        
    print("\nThank you for using our Sphere Volume Calculator!")