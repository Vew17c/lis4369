#!/usr/bin/env python3
def calculate_miles_per_gallon():
    miles = 0
    gallons = 0
    total = 0

    lck = True

    print("\nInput:")

    while lck == True:
        try:
            miles = float(input("Enter miles driven: "))
            lck = False
        except ValueError:
            print ("Incorrect value. Please enter a decimal number.\nExample: 300.0 or 300\n")

    lck = True
    while lck == True:
        try:
            gallons = float(input("Enter gallons of fuel used: "))
            lck = False
        except ValueError:
            print ("Incorrect value. Please enter a decimal number.\nExample: 20.0 or 20\n")

    total = miles/gallons

    print("\nOutput:")
    print('{:,.2f}'.format(round(miles , 2)) + " miles driven and " + '{:,.2f}'.format(round(gallons , 2)) + " gallons used = " + '{:,.2f}'.format(round(total , 2)) + " mpg")

def get_requirements():
    print("Miles Per Gallon \n\nProgram Requirements:")
    print("1. Convert MPG.\n2. Must use float data type for user input and calculation.\n3. Format and round conversion to two decimal places.")

    