 #!/usr/bin/env python3

def get_requirements():
    print("\nProgram Requirements:")
    print("1. Tuples (Python data structure): mutable, ordered sequence of elements.")
    print("2. Tuples are mutable/changeble--that is, can insert, update, delete.")
    print("3. Create tuples - using parentheses (tuple)): my_tuple1 = (cherries, apples, bananas, oranges).")
    print("4. Create a program that mirrors the following IPO (input/processing/output) format.")
    #print("4. Format, right-align numbers, and round to two decimal places")

def calculation():
    tupple_1 = ("cherries" , "apples" , "bananas" ,  "oranges")
    tupple_2 = 1, 2, "three", "four"

    print("\nOutput")
    print("Print my_tuple1: ")
    print(tupple_1)

    print("\nPrint my_tuple2: ")
    print(tupple_2)

    print("\nPrint my_tuple1 unpacking: ")
    fruit1, fruit2, fruit3, fruit4 = tupple_1
    print(fruit1, fruit2, fruit3, fruit4)

    print("\nPrint the third element in my_tuple2: ")
    print(tupple_2[2])

    print("\nPrint 'slice' of my_tuple1 (second and third elements): ")
    print(tupple_1[1:3])

    print("\nReasigning my_tuple2 using parentheses: ")
    tupple_2 = (1,2,3,4)
    print(tupple_2)

    print("\nReasigning my_tuple2 using packing methood (no parentheses): ")
    tupple_2 = 5, 6, 7, 8

    print("Print number of elements in my_tuple1: " + str(len(tupple_1)))

    print("Print type of my_tuple1: " + str(type(tupple_1)))

    print("Delete my_tuple \nNote: will generate error, if trying to print after, as it no longer exist.")
    del tupple_1
    #print(tupple_1)